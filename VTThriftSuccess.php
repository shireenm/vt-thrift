<?php
  $loginOK = false;
  $product1 = "";
  if(isset($_POST["submit"])){
    if(isset($_POST["product1"])) $product1=$_POST["product1"];
    if(isset($product1)){
      $loginOK = true;
    }
    if($loginOK) {
      session_start();
      $_SESSION["productSuccess"] = $product1;
      Header("Location:showProductSuccess.php");
    }
  }
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VT Thrift</title>
    <style media="screen">
        
    .errlabel     {color:red;}
    .centerImage      {display: block;
                        margin-left: auto;
                        margin-right: auto;
                        width: 500px;}
    .center           {margin: auto;
                        width: 640px;
                        border: 3px solid black;
                        padding: 10px;}
    .inputAlign       {margin: auto;
                        width: 370px;
                        border: 3px solid black;
                        padding: 10px;}
    .centerText       {margin: auto;
                        width: 460px;
                        border: 3px solid black;
                        padding: 10px;}
    .confirmationText {margin: auto;
                        width: 660px;
                        border: 3px solid black;
                        padding: 10px;}
    </style>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <a href="ProductDetails.php"><img src="SoleTakerHeaderLogo.png" class='centerImage'></a>
      <nav class='center'>
      <ul class="nav nav-pills">
            <li><a href="HomePage.php">Home</a></li>
            <li><a href="ProductDetails.php">Shop</a></li>
            <li><a href="HowToPage.php">How it Works</a></li>
            <li><a href="ModifyOrder.php">Modify Order</a></li>
            <li><a href="VTThriftSuccess.php">Previous Success</a></li>
            <li><a href="EmployeeLogin.php">Admin Login</a></li>
              </ul>
            </li>
          </ul>
      </nav>
        <br><br><br>
      <h1 class="centerText">Previous Success</h1>
      <br><br><br>
      <nav class="centerProd">
        <p><strong>Select the product you wish to view: </strong></p>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label>Product 1:</label>
        <?php
        require_once("db.php");
        $sql = "SELECT DISTINCT prod_name FROM success";
        $result = $mydb->query($sql);

        echo "<select name='product1'id='product1'>";
        while($row = mysqli_fetch_array($result)){
          $val = $row['prod_name'];
          echo "<option value='$val'>".$val."</option>";
        };
        echo "</select>";

        ?>
        <input type="submit" name="submit" value="Submit" />
      </form>
    </nav>
    </div>
    <br>
    <p align='center'>&copy;<small>2020 VT Thfift, Inc. All Rights Reserved.</small></p>
    <br>
  </body>
</html>