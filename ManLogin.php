<?php
     $ManID="0";
     $Mpwd="";
     $remember="no";
     $error= false;
     $loginOK= null;
 
     if(isset($_POST["submit"])){
         if(isset($_POST["ManID"])) $ManID=$_POST["ManID"];
         if(isset($_POST["ManPassword"])) $Mpwd=$_POST["ManPassword"];
         if(isset($_POST["remember"])) $remember=$_POST["remember"];
        
         // $hour = date("H");
         // $time = date("F j, Y,g:i a");
 
         if(empty($ManID) || empty($Mpwd)){
             $error=true; 
         }
         
 
         if(!$error){
             require_once("db.php");
             $sql = "select ManPassword from bit4444group13.manager where ManID='$ManID'";
             $result= $mydb->query($sql);
 
             $row=mysqli_fetch_array($result);
             if($row){
                 if(strcmp($Mpwd, $row["ManPassword"])==0){
                     $loginOK=true;
                 } else {
                     $loginOK = false;
                 }
             }
 
             if($loginOK){
                 session_start();
                 
                 $_SESSION["ManID"] = $ManID;
                 $_SESSION["ManPassword"] = $Mpwd;
                 setcookie("timestamp",date("F j, Y,g:i a"),time(),time()+60*60*24*2,"/");
                 date_default_timezone_set('America/New_York');
 
                 Header("HTTP/1.1 307 Temporary Redirect");
                 Header("Location:ManMain.html");
 
                 //set cookies to remeber timestamp after logging in
                 // date_default_timezone_set('US/Eastern');
                 // $date = date('Y-m-d h:i:sa', $_COOKIE['logtime']);
                 // setcookie("logtime",$_COOKIE['logtime'],time()+60*60*24*2,"/");
        
             }
         }
     }
?>

<!doctype html>
<html>
<head>
    <meta charset = "UTF-8">
    <meta name = "viewport" content="width=device-width,initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
    <title>Manager Login</title>
   

    <style type="text/css">

        /* body {Background-color: lightblue;} */

        .maroon{color: maroon;
            font-family: Arial Black;
            font-weight: 700;
            font-size: 19pt;

        }

        .errlabel {color:red}
       /* ul{
            list-style-type: none;
            margin: 0;
            padding: 0; 
            overflow: hidden;
            background-color: maroon; 
        } */

        /* li {
            float: right;
        } */

        li a{
            display: block;
            color:white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        } 

        /* li a:hover{
            background-color:orange ;
        } */

        /* .active{
            background-color:orange;
        } */

    </style>
<head>

<!-- <body>
<nav> -->
    <!--Use a space to separate class names -->
    <!-- <ul class="nav nav-pills" class="list-inline"> -->
    <!-- <ul>
        <li><a href="ContactUs.php">Contact Us</a></li>
        <li><a href="AboutUs.html">About Us</a></li>
        <li><a href="Review.php">Review</a></li>
        <li style="float:center"><a class="active" href="Account.html">Account</a></li>
        <li><a href="HomePage.php">Home</a></li>

    </ul> -->
  <!-- </nav>
</body> -->

<body>
           <!--Navigation bar-->
          <nav class="navbar navbar-light" style="background-color:#800000" role="navigation">
            <ul class="nav nav-pills">    
                <li><a href="HomePage.php">Home</a></li>
                <li class="active"><a href="#">Account</a></li>
                <li><a href="Review.php">Review</a></li>
                <li><a href="AboutUs.html">About Us</a></li>
                <li><a href="ContactUs.php">Contact Us</a></li>
            </ul>
          </nav>
        </body>

      
    </br>
      </br>

<body>
    <h1 class="maroon" style="width:240px; margin: 0px auto; ">Manager Login</h1>
    <!-- <section> -->
    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']?>">
    <fieldset class="forminputs" style="width:500px; margin: 0px auto; ">
    <table>
            <tr>
                <td>Enter ID</td>
            </tr>
            <tr>
                <td><input type="number" name="ManID" value="<?php if(!empty($ManID)) echo $ManID;?>" />
                    <?php if($error && $ManID<0) echo "<span class='errlabel'> Please enter your ID</span>"; ?> </td>
            </tr>
            <tr>
                <td>Enter Password</td>
            </tr>  
            <tr>
                <td><input type="password" type="hidden" name="ManPassword" value="<?php if(!empty($Mpwd)) echo $Mpwd;?>" />
                    <?php if($error && empty($Mpwd)) echo "<span class='errlabel'> Please enter a password</span>"; ?> </td>
            </tr> 
        
        </table>

        <table>
            <tr>
                <td><input type="checkbox" name="remember" value="yes"/><label>Remember me</label></td>
            </tr>
            <tr>
                <td><?php if(!is_null($loginOK) && $loginOK==false) echo "<span class='errlabel'> Manager ID and password do not match.</span>"; ?></td>
            </tr>
            </table>
        <br />
        <br />
        <br />
    
    <input style="width:200px; margin: 0px auto; " type="submit" name="submit" value="Sign In" />

    <br />
    </fieldset>
    <!-- </section> -->
    </form>  

    <?php include 'Footer.php';?>


    </body>
</html>

