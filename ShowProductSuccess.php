<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VT Thrift Product Success</title>
    <style media="screen">
    .centerText       {margin: auto;
                        width: 315px;
                        border: 3px solid black;
                        padding: 10px;}
    .centerImage      {display: block;
                        margin-left: auto;
                        margin-right: auto;
                        width: 500px;}
    .center           {margin: auto;
                        width: 640px;
                        border: 3px solid black;
                        padding: 10px;}
    .centerPageHead   {margin: auto;
                        width: 665px;
                        border: 3px solid black;
                        padding: 10px;}
    .row-centered     {text-align:center;}
    svg               {display: block;
                        margin: 0 auto;}
    </style>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <a href="SoleTakerHome.php"><img src="SoleTakerHeaderLogo.png" class='centerImage'></a>
      <nav class='center'>
      <ul class="nav nav-pills">
            <li><a href="HomePage.php">Home</a></li>
            <li><a href="ProductDetails.php">Shop</a></li>
            <li><a href="HowToPage.php">How it Works</a></li>
            <li><a href="ModifyOrder.php">Modify Order</a></li>
            <li><a href="VTThriftSuccess.php">Previous Success</a></li>
            <li><a href="EmployeeLogin.php">Admin Login</a></li>
              </ul>
            </li>
          </ul>
      </nav>
        <br><br><br>
      <h1 class="centerText">Previous Success</h1>
      <br><br><br>
      <svg width="960" height="500"></svg>
      <script src="https://d3js.org/d3.v4.min.js"></script>
      <script>

      var svg = d3.select("svg"),
          margin = {top: 20, right: 20, bottom: 30, left: 40},
          width = +svg.attr("width") - margin.left - margin.right,
          height = +svg.attr("height") - margin.top - margin.bottom;

      var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
          y = d3.scaleLinear().rangeRound([height, 0]);
          console.log(y(3));

      var g = svg.append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      //specify our data source and using that data for the bar chart
      d3.json("getSuccessData.php", function(error, data){
        if(error) throw error;

        data.forEach(function(d){
          d.letter = d.product;
          d.frequency = +d.totalSuccess;
        })

        console.log(data);

        if (error) throw error;

        x.domain(data.map(function(d) { return d.letter; }));
        y.domain([0, d3.max(data, function(d) { return d.frequency; })]);

        g.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        g.append("g")
            .attr("class", "axis axis--y")
            .call(d3.axisLeft(y).ticks(4, "s"))
          .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", "0.71em")
            .attr("text-anchor", "end")
            .text("Frequency");

        g.selectAll(".bar")
          .data(data)
          .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function(d) { return x(d.letter); })
            .attr("y", function(d) { return y(d.frequency); })
            .attr("width", x.bandwidth())
            .attr("height", function(d) { return height - y(d.frequency); });
      });
      </script>
    </div>
    <br>
    <p align='center'>&copy;<small>2020 VT Thrift, Inc. All Rights Reserved.</small></p>
    <br>
  </body>
</html>