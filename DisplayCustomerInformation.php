<?php
$FirstName = $_GET['FirstName'];
$CPassword = $_GET['CPassword'];
session_start();
$_SESSION['loginOK'] = false;

      //check supplierID and companyName with the database record
      require_once("db.php");
      $sql = "SELECT CID, email, FirstName, CPassword FROM customers";
      $result = $mydb->query($sql);

      while ($row = mysqli_fetch_array($result)) {
        if(strcmp($FirstName, $row["FirstName"]) == 0 && strcmp($CPassword, $row["CPassword"]) == 0){
          $customerId = $row['CID'];
          $_SESSION["customerID"] = $customerId;
          $customerEmail = $row['email'];
          $_SESSION["customerEmail"] = $customerEmail;
          $sql = "SELECT customer_first_name, customer_last_name, email, customer_street, customer_city, customer_zip FROM customers WHERE CID=$customerId";
          $result = $mydb->query($sql);
          $row=mysqli_fetch_array($result);
          echo "<table>";
            echo "<tr>";
              echo "<td>";
                echo "First Name: ";
              echo "</td>";
              echo "<td>";
                echo $row['customer_first_name'];
              echo "</td>";
            echo "</tr>";
            echo "<tr>";
              echo "<td>";
                echo "Last Name: ";
              echo "</td>";
              echo "<td>";
                echo $row['customer_last_name'];
              echo "</td>";
            echo "</tr>";
            echo "<tr>";
              echo "<td>";
                echo "Email: ";
              echo "</td>";
              echo "<td>";
                echo $row['email'];
              echo "</td>";
            echo "</tr>";
            echo "<tr>";
              echo "<td>";
                echo "Street: ";
              echo "</td>";
              echo "<td>";
                echo $row['customer_street'];
              echo "</td>";
            echo "</tr>";
            echo "<tr>";
              echo "<td>";
                echo "City: ";
              echo "</td>";
              echo "<td>";
                echo $row['customer_city'];
              echo "</td>";
            echo "</tr>";
            echo "<tr>";
              echo "<td>";
                echo "Zip: ";
              echo "</td>";
              echo "<td>";
                echo $row['customer_zip'];
              echo "</td>";
            echo "</tr>";
          echo "</table>";
          $_SESSION['loginOK'] = true;
          break;
        } else {
          $_SESSION['loginOK'] = false;
        }
      }

      if ($_SESSION['loginOK'] == false) {
        echo "Login Failure";
        echo "<br />";
        echo "Please ensure that you are entering the correct FirstName/CPassword combination.";
      }

?>