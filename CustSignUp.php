<body>
<?php
    $CID = 0;
    $custUser = "";
    $FName = "";
    $LName = "";
    $Email = "";
    $custStreet="";
    $custCity="";
    $custState="";
    $custZip="";
    $CPassword = "";
    $err = false;

    if (isset($_POST["submit"])) {
        if(isset($_POST["CID"])) $CID=$_POST["CID"];
        if(isset($_POST["customer_username"])) $custUser=$_POST["customer_username"];
        if(isset($_POST["FirstName"])) $FName=$_POST["FirstName"];
        if(isset($_POST["LastName"])) $LName=$_POST["LastName"];
        if(isset($_POST["Email"])) $Email=$_POST["Email"];
        if(isset($_POST["customer_street"])) $custStreet=$_POST["customer_street"];
        if(isset($_POST["customer_city"])) $custCity=$_POST["customer_city"];
        if(isset($_POST["customer_state"])) $custState=$_POST["customer_state"];
        if(isset($_POST["customer_zip"])) $custZip=$_POST["customer_zip"];
        if(isset($_POST["CPassword"])) $CPassword=$_POST["CPassword"];
      

        if(!empty($FName) && !empty($LName) && !empty($Email) && !empty($CPassword)) {
            header("HTTP/1.1 307 Temporary Redirect");
            header("Location: CustLogin.php");
        } else {
            $err = true;
        }    

       
        
     
    require_once("db.php");

        if($CID==0){
            $sql ="insert into bit4444group13.customers(customer_username,FirstName, LastName, Email, CPassword,customer_street,customer_city,customer_state,customer_zip) values ( '$custUser','$FName', '$LName', '$Email', '$CPassword', '$custStreet', '$custCity', '$custState', '$custZip')";
                //echo $sql;
                $result=$mydb->query($sql);

                if($result==1){
                    echo "<p>A customer record has been updated</p>";
                }
        } 

        else{
            $sql ="update bit4444group13.customers set customer_username = '$custUser', FirstName = '$FName', LastName = '$LName', Email= '$Email',  CPassword = '$CPassword' , customer_street = '$custStreet', customer_city= '$custCity' , customer_state='$custState', customer_zip = '$custZip' where CID = $CID";
            //echo $sql;
            $result=$mydb->query($sql);

            if($result==1){
                echo "<p>An customer record has been updated</p>";
            }
        }  

      
     
    }   
    
    
?>
</body>
 


<!doctype html>
<html>
<head>
    <meta charset = "UTF-8">
    <meta name = "viewport" content="width=device-width,initial-scale=1.0">
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script> 
    <script src="js/bootstrap.min.js"></script>
    <!--<script src="js/bootstrap.min.js"></script> -->
    <title>Create Customer Profile</title>
   

    <style type="text/css">
       
       /* body {Background-color: lightblue;} */

       .maroon{color: maroon;
            font-family: Arial Black;
            font-weight: 700;
            font-size: 19pt;
    
        }
       
       .errlabel {color:red}
        ul{
            list-style-type: none;
            margin: 0;
            padding: 0; 
            overflow: hidden;
            background-color: maroon; 
        }

        li {
            float: right;
        }

        li a{
            display: block;
            color:white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        }

        li a:hover{
            background-color: orange;
        }

        .active{
            background-color:orange;
        }

    </style>
<head>

<body>
           <!--Navigation bar-->
          <nav class="navbar navbar-light" style="background-color:#800000" role="navigation">
            <ul class="nav nav-pills">    
                <li><a href="HomePage.php">Home</a></li>
                <li><a href="Review.php">Review</a></li>
                <li><a href="AboutUs.html">About Us</a></li>
                <li><a href="ContactUs.php">Contact Us</a></li>
            </ul>
          </nav>
        </body>

      
    </br>
      </br>



<body>
    <h1 class="maroon" style="width:240px; margin: 0px auto; ">Create Customer Profile</h1>
    <section>
    <form name="profile" method="POST" action="<?php echo $_SERVER['PHP_SELF']?>">
    <fieldset style="width:500px; margin: 0px auto; ">
    <label>Customer ID: (Leave blank for new Customer)</label>
    <input type="number" name="CID" maxlength="4" size="4" value="<?php echo $CID; ?>"/>
    <br />
    <br />
    <br />

    <label>Customer Username: </label>
    <input name="customer_username" type="text" required  value="<?php echo $custUser; ?>"/>
    <?php
        if($err && empty($custUser)){
            echo "<label class='errlabel'>Error: Please enter the username</label>";
        }
    ?>
    <br />
    <br />
    <br />


    <label>First Name:</label>
    <input name="FirstName" type="text" required  value="<?php echo $FName; ?>"/>
    <?php
        if($err && empty($FName)){
            echo "<label class='errlabel'>Error: Please enter the first name of an customer</label>";
        }
    ?>
    <br />
    <br />
    <br />

    <label>Last Name:</label>
    <input name="LastName" type="text" required value="<?php echo $LName; ?>"/>
    <?php
        if($err && empty($LName)){
            echo "<label class='errlabel'>Error: Please enter the last name of a customer</label>";
        }
    ?>
    <br />
    <br />
    <br />

    <label>Customer Email:</label>
    <input name="Email" type="email" placeholder="name@domain.com" required value="<?php echo $Email; ?>"/>
    <?php
        if($err && empty($Email)){
            echo "<label class='errlabel'>Error: Please enter the customers email</label>";
        }
    ?>
    <br />
    <br />
    <br />

    <label>Password:</label>
    <input name="CPassword" type="password" type="hidden" required value="<?php echo $CPassword; ?>"/>
    <?php
        if($err && empty($CPassword)){
            echo "<label class='errlabel'>Error: Please enter a password</label>";
        }
    ?>
    <br />
    <br />
    <br />

    <label>Street Address:</label>
    <input name="customer_street" type="text" required  value="<?php echo $custStreet; ?>"/>
    <?php
        if($err && empty($custStreet)){
            echo "<label class='errlabel'>Error: Please enter your street address</label>";
        }
    ?>
    <br />
    <br />
    <br />

    <label>Customer City Address:</label>
    <input name="customer_city" type="text" required  value="<?php echo $custCity; ?>"/>
    <?php
        if($err && empty($custCity)){
            echo "<label class='errlabel'>Error: Please enter your city</label>";
        }
    ?>
    <br />
    <br />
    <br />


    <label>Customer State:</label>
    <input name="customer_state" type="text" required  value="<?php echo $custState; ?>"/>
    <?php
        if($err && empty($custState)){
            echo "<label class='errlabel'>Error: Please enter your state</label>";
        }
    ?>
    <br />
    <br />
    <br />

    <label>Customer Zip Code:</label>
    <input name="customer_zip" type="text" required  value="<?php echo $custZip; ?>"/>
    <?php
        if($err && empty($custZip)){
            echo "<label class='errlabel'>Error: Please enter your Zip</label>";
        }
    ?>
    <br />
    <br />
    <br />

    
    
    <input style="width:200px; margin: 0px auto; " type="submit" name="submit" value="Submit"  />
    <!--onclick="submit_form();" />-->
    <!-- <input style="width:200px; margin: 0px auto; " type="reset"  name="refresh" value="Clear" /> -->

    <br />
    </fieldset>
    </form>
    </section>
    </body>
</html>

