<!DOCTYPE html>
<html>
  <head>
    <meta charset = "UTF-8">
    <meta name = "viewport" content="width=device-width,initial-scale=1.0">
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <script src="jquery-3.1.1.min.js"></script> 
    <script src="js/bootstrap.min.js"></script> 
    <title>Link To Products & Ordering</title>
  </head>
  <style>
    .errlabel {color:red}
    ul{
        list-style-type: none;
        margin: 0;
        padding: 0; 
        overflow: hidden;
        background-color: maroon; 
    }

    li {
        float: right;
    }

    li a{
        display: block;
        color:white;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;

    }

    li a:hover{
        background-color: orange;
    }

    .active{
        background-color:orange;
    }

    .clothing-img {
      width: 100vw;
    }


</style>


<body>
    <nav>
        <!--Use a space to separate class names -->
        <!-- <ul class="nav nav-pills" class="list-inline"> -->
        <ul>
                <li><a href="AboutUs.html">AboutUs</a></li>
                  <li><a href="Reviews.html">Review</a></li>
                  <li style="float:center"><a class="active" href="Account.html">Account</a></li>
                  <li><a href="HomePage.php">Home</a></li>
        </ul>
      </nav>
    
  <body>
    <div class="btn-group">

        <button onclick="window.location.href = 'ProductDetails.php';" type="button" id="button1" >Products Available</button>
        
        <button onclick="window.location.href = 'SearchProducts.html';" type="button" id="button2">Search for Products</button>
        
        <button onclick="window.location.href = 'OrderDetailspage.php';" type="button" id="button3">View/Modify Order</button>
      
        </div>
  </body>
</html>