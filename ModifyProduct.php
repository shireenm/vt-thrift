<?php  
    $prodID = 0;
    $prodName = "";
    $prodDesc="";
    $InStock = 0;
    $prodDesc="";
    $pprice=0;
    $DateP="2020-12-01";
    $Pimg="";
    $UnitWeight=0;

    $err = false;

    require_once("db.php");

if (isset($_POST["Modify"])) {
      if(isset($_POST["PID"])) $prodID=$_POST["PID"];
      if(isset($_POST["PName"])) $prodName=$_POST["PName"];
      if(isset($_POST["PDescription"])) $prodDesc=$_POST["PDescription"];
      if(isset($_POST["PPrice"])) $pprice=$_POST["PPrice"];
      if(isset($_POST["InStockQuantity"])) $InStock=$_POST["InStockQuantity"];
      if(isset($_POST["DateOfPurchase"])) $DateP=$_POST["DateOfPurchase"];
      // if(isset($_POST["PImgFileName"])) $PImg=$_POST["PImgFileName"];
      if(isset($_POST["UnitWeight"])) $UnitWeight=$_POST["UnitWeight"];

        //get the file name
        if(isset($_FILES['image'])){
          $errors= array();
          $file_name = $_FILES['image']['name'];
          $file_size = $_FILES['image']['size'];
          $file_tmp = $_FILES['image']['tmp_name'];
          $file_type = $_FILES['image']['type'];
          $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
          
          $extensions= array("jpeg","jpg","png");
          
          if(in_array($file_ext,$extensions)=== false){
             $errors[]="extension not allowed, please choose a JPEG or PNG file.";
          }
          
          if($file_size > 2097152) {
             $errors[]='File size must be excately 2 MB';
          }
          
          if(empty($errors)==true) {
            $PImg = $file_name;
             move_uploaded_file($file_tmp,"images/".$file_name);
             echo "Success";
          }else{
             print_r($errors);
          }
       }

      if(!empty($prodName) && !empty($prodDesc) && !empty($pprice) && !empty($pprice) && !empty($InStock) 
      && !empty($DateP) && !empty($PImg) && $UnitWeight>0) { 
        header("HTTP/1.1 307 Temprary Redirect"); 
         header("Location: ProdDatabase.php");
      } else {
        $err = true;
      }
}

         require_once("db.php");
  
        if($prodID!=0) {
                $sql ="update bit4444group13.product set PName = '$prodName', PDescription = '$prodDesc', PPrice= $pprice,  InStockQuantity = $InStock, DateOfPurchase = '$DateP', PImgFileName = '$PImg',
                 UnitWeight=$UnitWeight where PID = $prodID";
                
                $result=$mydb->query($sql);

                if($result==1){ 
                  echo "<p>A product record has been updated</p>";
              }
            }
  
    ?>

<!doctype html>
<html>
<html lang="en" dir="ltr">
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0" >
<link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script> 
<head>
    <title>Modify Product</title>
    <style type="text/css">
       
       /* body {Background-color: lightblue;} */

       .maroon{color: maroon;
            font-family: Arial Black;
            font-weight: 700;
            font-size: 19pt;
    
        }
       
       .errlabel {color:red}
        /* ul{
            list-style-type: none;
            margin: 0;
            padding: 0; 
            overflow: hidden;
            background-color: maroon; 
        } */

        /* li {
            float: right;
        } */

        li a{
            display: block;
            color:white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        } 

        /* li a:hover{
            background-color:orange ;
        } */

        /* .active{
            background-color:orange;
        } */
    </style>
</head>

<!-- <body>
<nav> -->
    <!--Use a space to separate class names -->
    <!-- <ul class="nav nav-pills" class="list-inline"> -->
    <!-- <ul>
      <li><a href="HomePage.php">Home</a></li>
      <li><a href="Reviews.html">Review</a></li>
      <li><a href="AboutUs.html">About Us</a></li>
      <li><a href="ContactUs.php">Contact Us</a></li>

    </ul> -->
  <!-- </nav>
</body> -->


<body>
           <!--Navigation bar-->
          <nav class="navbar navbar-light" style="background-color:#800000" role="navigation">
            <ul class="nav nav-pills">    
                <li><a href="HomePage.php">Home</a></li>
                <li><a href="Review.php">Review</a></li>
                <li><a href="AboutUs.html">About Us</a></li>
                <li><a href="ContactUs.php">Contact Us</a></li>
                <li><a href="EmpLogout.php">Log Out</a></li>
            </ul>
          </nav>
        </body>

      
    </br>
      </br>

<body>
    <h1 class="maroon" style="width:240px; margin: 0px auto; ">Modify Product</h1>
    <form method = "post" action = "<?php echo $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
    <fieldset style="width:500px; margin: 0px auto; ">

    <label> Product ID (leave it blank for new product): </label>
    <input type = "number" name ="PID" value="<?php if(!empty($prodID) && $prodID>0) echo $prodID; ?>" />
    <br/>
    <br />
    <br />

    <label> Product Name: </label> 
        <input name = "PName" type = "text" value="<?php echo $prodName; ?>"/>
        <?php
         
         if ($err && empty($prodName)) {
            echo "<label class='errlabel'>Error: Please enter a product name</label>";
            //echo $sql
      }
?>
 <br />
  <br />
  <br />
     <label> Product Description: </label>  
      <input name = "PDescription" type = "text" value="<?php echo $prodDesc; ?>"/>
      <?php
        if ($err && empty($prodDesc)) {
          echo "<label class='errlabel'>Error: Please enter a product description</label>"; 
          
    }

?>
  <br />
  <br />
  <br />
<label> Product Price: </label>  
      <input name = "PPrice" type = "number" step = "0.01" value="<?php echo $pprice; ?>"/>
      <?php
        if ($err && empty($pprice)) {
          echo "<label class='errlabel'>Error: Please enter a product price</label>"; 
          
    }

?>
  <br />  
  <br />
  <br />
<label> Product Quantity: </label> 
        <input name = "InStockQuantity" type = "number" value="<?php echo $InStock; ?>"/>
        <?php
          if ($err && $InStock<0) {
            echo "<label class='errlabel'>Error: Please enter Quantity</label>"; 
      }
  ?>
  <br />
  <br />
  <br />

  <label> Date Of Purchase: </label> 
        <input name = "DateOfPurchase" type = "date" value="<?php echo $DateP; ?>"/>
        <?php
           if ($err && empty($DateP)) {
            echo "<label class='errlabel'>Error: Please enter a Date</label>"; 
      } 
  ?>
  <br />
  <br />
  <br />

 
  <label> Product Image (leave blank for existing product): </label> <br/>
  <input type="file" name="image" />

      
  <!-- Select image to upload:
  <input type="file" name="PImgFileName" id="PImgFileName" value="<?php echo $PImg; ?>">
  <input type="submit" value="Upload Image" name="submit">
</form> -->
        <!-- Select image to upload:
        <input name = "PImgFileName" type = "file" id = "PImgFileName" value="<?php echo $PImg; ?>"/> -->
  
  
  <br />
  <br />
  <br />
  <label> Unit Weight: </label> 
        <input name = "UnitWeight" type = "number" value="<?php echo $UnitWeight; ?>"/>
        <?php
          if ($err && $UnitWeight<0) {
            echo "<label class='errlabel'>Error: Please enter the unit weight</label>"; 
      }
  ?>
  <br />
  <br />
  <br />
  
    <input style="width:200px; margin: 0px auto; " type="submit"  name="Modify" value="Modify" />

    <br />
    </fieldset>
    </form>
    </section>

    <a href="ProdMain.html">Return to main page</a>
</body>
  </html>