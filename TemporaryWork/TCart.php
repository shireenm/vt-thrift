<?php
$imgSource = "";
$username = "";
$password = "";
$PID = "";
$CCNumber = "";
$CCSecurityCode = "";
$CCExp = "";
$CCFName = "";
$CCLName = "";
$err = false;
    session_start();
    $PPrice = $_SESSION['PPrice'];
    if (isset($_POST["submitOrder"])) {
        if(isset($_POST["CCFName"])) $CCFName=$_POST["CCFName"];
        if(isset($_POST["CCLName"])) $CCLName=$_POST["CCLName"];
        if(isset($_POST["CCNumber"])) $CCNumber=$_POST["CCNumber"];
        if(isset($_POST["CCSecurityCode"])) $CCSecurityCode=$_POST["CCSecurityCode"];
        if(isset($_POST["CCExp"])) $CCExp=$_POST["CCExp"];

        $_SESSION['CCFName'] = $CCFName;
        $_SESSION['CCLName'] = $CCLName;
        $_SESSION['CCNumber'] = $CCNumber;
        $_SESSION['CCSecurityCode'] = $CCSecurityCode;
        $_SESSION['CCExp'] = $CCExp;
        $PID = $_SESSION['PID'];
        $customerId = $_SESSION['customerID'];
        if(!empty($CCFName) && !empty($CCLName) && !empty($CCNumber) && !empty($CCSecurityCode)
            && !empty($CCExp) && !empty($customerId) && !empty($PID) && strlen($CCNumber) == 16
            && strlen($CCExp) == 5 && strlen($CCSecurityCode) == 3) {
          header("HTTP/1.1 307 Temprary Redirect"); //
          header("Location: TPlaceOrder.php");
        } else {
          $err = true;
        }
    }

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Cart</title>
  <style>
    .errlabel     {color:red;}
    .center       {margin: auto;
                    width: 640px;
                    border: 3px solid black;
                    padding: 10px;}
    .centerText   {margin: auto;
                    width: 100px;
                    border: 3px solid black;
                    padding: 10px;}
    .centerImage  {display: block;
                    margin-left: auto;
                    margin-right: auto;
                    width: 500px;}
    .cartInformation   {margin-left: 1250px;
                          margin-top: 25px;
                          width: 400px;
                          border: 3px solid black;
                          padding: 10px;}
    .checkoutInformation {margin-left: 1250px;
                            margin-top: -110px;
                            width: 400px;
                            border: 3px solid black;
                            padding: 10px;}
    .loginLabel             {margin-left: 1250px;
                              margin-top: 25px;
                              width: 400px;
                              border: 3px solid black;
                              padding: 10px;}
    .loginForm             {margin-left: 1250px;
                              margin-top: 25px;
                              width: 400px;
                              border: 3px solid black;
                              padding: 10px;}
    #customerInformation   {margin-left: 1250px;
                              display: none;
                              margin-top: 25px;
                              width: 400px;
                              border: 3px solid black;
                              padding: 10px;}
    .paymentInformation    {margin-left: 1250px;
                              margin-top: 25px;
                              width: 400px;
                              border: 3px solid black;
                              padding: 10px;}
    .loginButtons          {margin-left: 70px;}
    .custInformation       {margin-left: 1250px;
                              display: none;
                              margin-top: 25px;
                              width: 400px;
                              border: 3px solid black;
                              padding: 10px;}
    .PPrice                 {margin-left: 1150px;
                              margin-top: -650px;
                              width: 80px;
                              border: 3px solid black;
                              padding: 10px;}
                              .prodImage {margin-top: -60px;}
  </style>
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="jquery-3.1.1.min.js"></script>
  <script>
    $(document).ready(function(){
      $("#login").click(function(){
        var username = $("#username").val();
        var password = $("#password").val();
        $('.custInformation').css("display", "block");
        $('#customerInformation').css("display", "block");
        $.ajax({
          url:"displayCustomerInformation.php",
          data: 'username=' + username + '&password=' + password,
          async:true,
          success: function(data){
            $("#customerInformation").html(data);
          }
        });
      });
    });
  </script>
</head>
  <body>
    <?php
      $imgSource = $_SESSION['imgSource'];
      $PID = $_SESSION['PID'];
    ?>
    <div class="container-fluid">
    <a href="VT Thrift.php"><img src="VT Thrift Logo.jpg" class='centerImage'></a>
    <nav class='center navbar-expand-sm bg-dark navbar-dark'>
        <ul class="nav nav-pills">
          <li><a href="TModify.php">Modify Order</a></li>
          <li><a href="AdminLogin.php">Admin Login</a></li>
            </ul>
          </li>
        </ul>
      </nav>
      <br><br><br>
      <h1 class="centerText">Cart</h1>
      <br><br><br>
      <img src="<?php echo $imgSource?>" width="50%" height="50%" class='prodImage'>
      <div class="PPrice">
        <h3>Price:</h3>
        <?php echo "<h4>$$PPrice</h4>" ?>
      </div>
      <div class="checkoutInformation">
        <p>Add to Cart Information</p>
      </div>
      <div class="cartInformation">
        <table>
              <tr>
                <td>Phone Number: </td>
                <td>
                  <?php echo $phoneNumber;?>
                </td>
              </tr>
              <tr>
                <td>Size: </td>
                <td>
                  <?php echo $size?>
                </td>
              </tr>
              <tr>
                <td>Carrier: </td>
                <td>
                  <?php echo $carrier;?>
                </td>
              </tr>
            </table>
      </div>
      <div class='loginLabel'>
        <p>Customer Login</p>
      </div>
      <div class="loginForm">
          <table>
            <tr>
              <td>Username: </td>
              <td><input type="text" name="username" id="username">
            </td>
            </tr>
            <tr>
              <td>Password: </td>
              <td><input type="text" name="password" id="password">
            </td>
            </tr>
            <tr></tr>
        </table>
        <div class="loginButtons">
          <input id="login" type="button" name="login" value="Login" />
          <a href="SoleTakerCreateAccount.php" style='color:black'> <input id="create" type="button" name="create" value="Create Account" /></a>
        </div>
      </div>
      <div class="custInformation">
        <p>Customer Information</p>
      </div>
      <div id="customerInformation">
        &nbsp;
      </div>
      <div class="paymentInformation">
        <table>
          <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
            <table>
              <tr>
                <td>Credit Card First Name: </td>
                <td><input type="text" name="CCFName">
                <?php if($err && empty($CCFName)) echo "<br /><span class='errlabel'> Please enter a first name.</span>"; ?>
              </td>
              </tr>
              <tr>
                <td>Credit Card First Name: </td>
                <td><input type="text" name="CCLName">
                <?php if($err && empty($CCLName)) echo "<br /><span class='errlabel'> Please enter a last name.</span>"; ?>
              </td>
              </tr>
              <tr>
                <td>Credit Card Number: </td>
                <td><input type="text" name="CCNumber">
                <?php if($err && empty($CCNumber)) echo "<br /><span class='errlabel'> Please enter a credit card number.</span>"; ?>
                <?php if($err && strlen($CCNumber) != 16) echo "<br /><span class='errlabel'> Please enter a valid credit card number.</span>"; ?>
              </td>
              </tr>
              <tr>
                <td>Credit Card Security Code: </td>
                <td><input type="text" name="CCSecurityCode">
                <?php if($err && empty($CCSecurityCode)) echo "<br /><span class='errlabel'> Please enter a security code.</span>"; ?>
                <?php if($err && strlen($CCSecurityCode) != 3) echo "<br /><span class='errlabel'> Please enter a valid security code.</span>"; ?>
              </td>
              </tr>
              <tr>
                <td>Credit Card Expiration: </td>
                <td><input type="text" name="CCExp" placeholder="mm-yy">
                <?php if($err && empty($CCExp)) echo "<br /><span class='errlabel'> Please enter an expiration date.</span>"; ?>
                <?php if($err && strlen($CCExp) != 5) echo "<br /><span class='errlabel'> Please enter a valid expiration date.</span>"; ?>
              </td>
              </tr>
              <tr>
                <td></td>
                <td>
                  <?php if($err && empty($customerId)) echo "<br /><span class='errlabel'> Please login to your customer account to place an order.</span>"; ?>
                  <input id='submitOrder' type="submit" name="submitOrder" value="Place Order">
                </td>
              </tr>
            </table>
          </form>
      </div>
    </div>
    <br><br><br><br><br>
    <p align='center'>&copy;<small>2020 VTThrift, Inc. All Rights Reserved.</small></p>
    <br>
  </body>
</html>