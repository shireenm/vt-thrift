<?php
  $loginOK = false;
  $prod_name = "";
  if(isset($_POST["submit"])){
    if(isset($_POST["prod_name"])) $prod_name=$_POST["prod_name"];
    if(isset($prod_name)){
      $loginOK = true;
    }
    if($loginOK) {
      session_start();
      $_SESSION["productSuccess"] = $prod_name;
      Header("Location:showSuccess.php");
    }
  }
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VT Thrift</title>
    <style media="screen">
    .centerText       {margin: auto;
                        width: 315px;
                        border: 3px solid black;
                        padding: 10px;}
    .centerImage      {display: block;
                        margin-left: auto;
                        margin-right: auto;
                        width: 500px;}
    .center           {margin: auto;
                        width: 640px;
                        border: 3px solid black;
                        padding: 10px;}
    .centerPageHead   {margin: auto;
                        width: 665px;
                        border: 3px solid black;
                        padding: 10px;}
    .row-centered     {text-align:center;}
    .centerProd       {margin: auto;
                        width: 380px;
                        border: 3px solid black;
                        padding: 10px;}
    </style>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <a href="HomePage.php"></a>
      <nav class='center'>
          <ul class="nav nav-pills">
            <li><a href="HomePage.php">Home</a></li>
            <li><a href="TProducts.php">Shop</a></li>
            <li><a href="TModify.php">Modify Order</a></li>
            <li class='active'><a href="TSuccess.php">Previous Success</a></li>
            <li><a href="AdminLogin.php">Admin Login</a></li>
              </ul>
            </li>
          </ul>
      </nav>
        <br><br><br>
      <h1 class="centerText">Previous Success</h1>
      <br><br><br>
      <nav class="centerProd">
        <p><strong>Select the product you wish to view: </strong></p>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label>Product 1:</label>
        <?php
        require_once("db.php");
        $sql = "SELECT DISTINCT prod_name FROM success";
        $result = $mydb->query($sql);

        echo "<select name='prod_name'id='prod_name'>";
        while($row = mysqli_fetch_array($result)){
          $val = $row['prod_name'];
          echo "<option value='$val'>".$val."</option>";
        };
        echo "</select>";

        ?>
        <input type="submit" name="submit" value="Submit" />
      </form>
    </nav>
    </div>
    <br>
    <p align='center'>&copy;<small>2019 Sole Taker, Inc. All Rights Reserved.</small></p>
    <br>
  </body>
</html>