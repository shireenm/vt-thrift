<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VT Thrift</title>
    <style media="screen">
    .centerText       {margin: auto;
                        width: 170px;
                        border: 3px solid black;
                        padding: 10px;}
    .centerImage      {display: block;
                        margin-left: auto;
                        margin-right: auto;
                        width: 500px;}
    .center           {margin: auto;
                        width: 640px;
                        border: 3px solid black;
                        padding: 10px;}
    .row-centered     {text-align:center;}
    </style>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <a href="HomePage.php"></a>
      <nav class='center'>
          <ul class="nav nav-pills">
            <li><a href="HomePage.php">Home</a></li>
            <li class='active'><a href="TProducts.php">Shop</a></li>
            <li><a href="TModify.php">Modify Order</a></li>
            <li><a href="VTThriftSuccess.php">Previous Success</a></li>
            <li><a href="AdminLogin.php">Admin Login</a></li>
              </ul>
            </li>
          </ul>
        </nav>
        <br><br><br>
      <h1 class="centerText">Products</h1>
        <br><br><br>
        <div class="row row-centered">
          <div class="col-xs-12 col-sm-6 col-md-4">
            <a href="AirJordanIVBred.php"><img src="ProductImages/HokieBobbleHead.jpg" height="350" width="450"></a>
            <h3>HokieBird Bobble Head</h3>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <a href="VTSweats.php"><img src="ProductImages/VTSweats.jpg" height="350" width="450"></a>
            <h3>VTSweats</h3>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <a href="VTLegoSet.php"><img src="ProductImages/VTLegoSet.jpg" height="350" width="450"></a>
            <h3>Lane Stadium Lege Set</h3>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <a href="VTSweats".php><img src="ProductImages/VTLoungeChair.jpg" height="350" width="450"></a>
            <h3>VT Lounge Chair</h3>
          </div>
        </div>
      </div>
      <br>
      <p align='center'>&copy;<small>2020 VT Thrift, Inc. All Rights Reserved.</small></p>
      <br>
  </body>
</html>