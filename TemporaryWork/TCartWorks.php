<?php

session_start();
$pid = 0;
if (isset($_GET['pid'])) $pid = $_GET['pid'];

require_once("db.php");
$sql = "";
if(isset($_POST["add_to_cart"])){
  if(isset($_SESSION["shopping_cart"])){
    $item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
    if(!in_array($_GET["PID"], $item_array_id)){
      $count = count($_SESSION["shopping_cart"]);
      $item_array = array(
        'item_id' => $_GET["PID"],
        'item_name' => $_POST["hidden_name"],
        'item_price' => $_POST["hidden_price"],
        'item_quantity' => $_POST["quantity"]
      );
      $_SESSION["shopping_cart"][$count] = $item_array;
    }
    else{
      echo '<script>alert("Item Already Added")</script>';
      echo '<script>window.location="Checkout.php"</script>';
    }
  }
  else{
    $item_array = array(
      'item_id' => $_GET["PID"],
      'item_name' => $_POST["hidden_name"],
      'item_price' => $_POST["hidden_price"],
      'item_quantity' => $_POST["quantity"]
    );
    $_SESSION["shopping_cart"][0] = $item_array;
  }
}

if(isset($_GET["action"])){
    if($_GET["action"] == "delete"){
        foreach($_SESSION["shopping_cart"] as $keys => $values){
            if($values["item_id"] == $_GET["PID"]){
                unset($_SESSION["shopping_cart"][$keys]);
                echo '<script>alert("Item Removed")</script>';
                echo '<script>window.location="Checkout.php"</script>';
            }
        }
    }
}
?>
<!doctype html>
<html>
  <head>
    <title>PHP Ajax</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  </head>

  <body>
  <!--<a id="productLink" href="">All product</a><br />
      <label>Choose a Product: &nbsp;&nbsp;
      <select name="PID" id="productDropDown" onchange="getContent()"> -->
        <!-- <?php
            /*
            $sql="SELECT PID FROM product ORDER BY PID";
            $result = $mydb->query($sql);
        
            while($row=mysqli_fetch_array($result)){
              echo "<option value=' ".$row["PID"]."'>".$row["PID"]."</option>"; 
              
            } */
            ?> 
        </select>
        </label><br /> -->
  
            <?php
                if ($pid == 0) {
                  $sql = "SELECT PID, PName, PPrice, InStockQuantity, PIMGFileName FROM product";
              } else {
                
                  $sql = "SELECT PID, PName, PPrice, InStockQuantity, PIMGFileName FROM product where PID =" . $pid;
              }
              $result = $mydb->query($sql);
                if(mysqli_num_rows($result) > 0){
              while($row = mysqli_fetch_array($result)){

            ?>
             <div class="container-fluid">
                <div class="col-md-3">
                  <form method="post" action="Checkout.php?add&id=<?php echo $row["PID"]; ?>">
                  <h4 class="text-info"><?php echo $row["PName"]; ?></h4>
                  <h4 class="text-danger">$ <?php echo $row["PPrice"]; ?></h4>
                  <input type="text" name="quantity" class="form-control" value="1" />
                  <input type="hidden" name="hidden_name" value="<?php echo $row["PName"]; ?>" />
                  <input type="hidden" name="hidden_price" value="<?php echo $row["PPrice"]; ?>" />
                  <input type="submit" name="add_to_cart" style="margin-top:5px" class="btn btn-success" value="Add to Cart" />
                  </div>
                  </form>
            </div>
            <?php
            }
          }

        ?>
        <div style ="clear:both"></div>
        <br />
        <h3>Order Details</h3>
        <div class="table-responsive">
          <table class="table table-bordered">
              <tr>
                <th width="40%">Item Name</th>
                <th width="10%">Quantity</th>
                <th width="20%">Price</th>
                <th width="15%">Total</th>
                <th width="5%">Action</th>
              <tr>
                <?php
                if(!empty($_SESSION["shopping_cart"])){
                  $total = 0;
                  foreach($_SESSION["shopping_cart"] as $keys => $values){
                ?>
                <tr>
                    <td><?php echo $values["item_name"]; ?></td>
                    <td><?php echo $values["item_quantity"]; ?></td>
                    <td><?php echo $values["item_price"]; ?></td>
                    <td><?php echo number_format($values["item_quantity"] * $values["item_price"], 2); ?></td>
                    <td><a href="Checkout.php?action=delete&id=<?php echo $values["item_id"]; ?>"><span class="text-danger">Remove</span></a></td>
                </tr>
                <?php
                      $total = $total + ($values["item_quantity"] * $values["item_price"]);
                  }
                ?>
                <tr>
                  <td colspan="3" align="right">Total</td>
                  <td align="right">$ <?php echo number_format($total, 2); ?></td>
                  <td></td>
                </tr>
                <?php
                }
                ?>
          </table>
        </div>
        </div>
        <br />
    <div id="contentArea">&nbsp;</div>
    <!--<button type ="button" id="btnAdd" value="Add Item" onclick="addItem()" >Add Item</button> -->
  </body>
</html>
