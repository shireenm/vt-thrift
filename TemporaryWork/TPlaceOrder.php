<?php
  session_start();
  $payment_cc_first_name = $_SESSION['payment_cc_first_name'];
  $payment_cc_last_name = $_SESSION['payment_cc_last_name'];
  $payment_cc_number = $_SESSION['payment_cc_number'];
  $CCSecurityCode = $_SESSION['CCSecurityCode'];
  $CCExp = $_SESSION['CCExp'];
  $OID = $_SESSION['OID'];
  $CID = $_SESSION['customerID'];
  $DateOfPurchase = $_SESSION["DateOfPurchase"];
  $TotalPreTaxCosts = $_SESSION['TotalPreTaxCosts'];
  $TotalTax = $_SESSION['TotalTax'];
  $TotalQuantity = $_SESSION['TotalQuantity'];
  $carrier = $_SESSION['carrier'];
  $size = $_SESSION['size'];
  $OID = $_SESSION['OID'];
  $price = $_SESSION['price'];


  require_once("db.php");
  $sql = "INSERT INTO payment(payment_cc_first_name, payment_cc_last_name, payment_cc_number, payment_cc_security_code, payment_cc_exp_date)
            VALUES('$payment_cc_first_name', '$payment_cc_last_name', $payment_cc_number, '$CCSecurityCode', '$CCExp')";
  $result=$mydb->query($sql);
  $paymentID = mysqli_insert_id($mydb->dbConn);

  $sql = "INSERT INTO orders(payment_id, customer_id, product_id, order_nike_email, order_nike_password, order_shoe_size, order_phone_number, order_price)
            VALUES('$paymentID', '$CID', '$OID', '$TotalPreTaxCosts', '$TotalTax', '$size', '$TotalQuantity', '$price')";
  $result=$mydb->query($sql);
  $OID = mysqli_insert_id($mydb->dbConn);

  switch ($carrier) {
    case 'AT&T':
      $textAlertAddress = $TotalQuantity."@txt.att.net";
      break;
    case 'Verizon':
      $textAlertAddress = $TotalQuantity."@vtext.com";
      break;
    case 'Sprint':
      $textAlertAddress = $TotalQuantity."@messaging.sprintpcs.com";
      break;
    case 'TMobile':
      $textAlertAddress = $TotalQuantity."@tmomail.net";
      break;

    default:
      break;
  }


  use PHPMailer\PHPMailer\PHPMailer;

  require_once('PHPMailer/src/PHPMailer.php');
  require_once('PHPMailer/src/SMTP.php');
  require_once('PHPMailer/src/Exception.php');

  $mail = new PHPMailer();
  $mail->isSMTP();
  $mail->SMTPAuth = true;
  $mail->SMTPSecure = 'ssl';
  $mail->Host = 'smtp.gmail.com';
  $mail->Port = '465';
  $mail->isHTML();
  $mail->Username = 'pm6345@gmail.com';
  $mail->Password = 'biggerthanme123';
  $mail->SetFrom('no-reply@soletaker.com', 'Sole Taker');
  $mail->Subject = 'Order Confirmation';
  $mail->Body = "Thank you for your order, order number ".$OID.". You will
                  be receiving text and email alerts for this order.";
  $mail->AddAddress($textAlertAddress);
  $mail->AddAddress($DateOfPurchase);
  $mail->send();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Order Confirmation</title>
    <style>
    .center           {margin: auto;
                        width: 640px;
                        border: 3px solid black;
                        padding: 10px;}
    .centerText       {margin: auto;
                        width: 340px;
                        border: 3px solid black;
                        padding: 10px;}
    .centerImage      {display: block;
                        margin-left: auto;
                        margin-right: auto;
                        width: 500px;}
    .confirmationText  {margin: auto;
                        width: 50%;
                        border: 3px solid black;
                        padding: 10px;}

    </style>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <a href="SoleTakerHome.php"><img src="SoleTakerHeaderLogo.png" class='centerImage'></a>
      <nav class='center'>
          <ul class="nav nav-pills">
            <li><a href="SoleTakerHome.php">Home</a></li>
            <li><a href="SoleTakerProducts.php">Shop</a></li>
            <li><a href="SoleTakerWorks.php">How it Works</a></li>
            <li><a href="SoleTakerModifyOrder.php">Modify Order</a></li>
            <li><a href="SoleTakerSuccess.php">Previous Success</a></li>
            <li><a href="AdminLogin.php">Admin Login</a></li>
              </ul>
            </li>
          </ul>
        </nav>
        <br><br><br>
      <h1 class="centerText">Order Confirmation</h1>
      <br><br><br>
      <?php
        echo "<p class='confirmationText'>
                Congradulations! You have successfully placed your order. Your order number is $OID.
                A copy of your order confirmation has been sent to $DateOfPurchase and $TotalQuantity.
                Please keep this number as you will need it to modify/delete your order.
                We are looking forward to securing your kicks!
              </p>";
      ?>
    </div>
    <br>
    <p align='center'>&copy;<small>2019 Sole Taker, Inc. All Rights Reserved.</small></p>
    <br>
  </body>
</html>