<!doctype html>
<html>
<head>
    <meta charset = "UTF-8">
    <meta name = "viewport" content="width=device-width,initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet"> 
    <script src="jquery-3.1.1.min.js"></script> 
    <script src="js/bootstrap.min.js"></script> 
    <title>Product Details</title>

    <style type="text/css">
       
       /* body {Background-color: lightblue;} */

       .maroon{color: maroon;
            font-family: Arial Black;
            font-weight: 700;
            font-size: 19pt;
    
        }
       
       .errlabel {color:red}
        ul{
            list-style-type: none;
            margin: 0;
            padding: 0; 
            overflow: hidden;
            background-color: maroon; 
        } */

         li {
            float: right;
        } 

        li a{
            display: block;
            color:white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        } 

        /* li a:hover{
            background-color:orange ;
        } */

        /* .active{
            background-color:orange;
        } */

    </style>
</head>
    <!-- <style>
        .errlabel {color:red}
        ul{
            list-style-type: none;
            margin: 0;
            padding: 0; 
            overflow: hidden;
            background-color: maroon; 
        }

        li {
            float: right;
        }

        li a{
            display: block;
            color:white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        }

        li a:hover{
            background-color: orange;
        }

        .active{
            background-color:orange;
        }

        .clothing-img {
          width: 100vw;
        }

    </style>
<head> -->

<body>
<script>
function goBack() {
  window.history.back()
}
</script>
<nav>
    <!--Use a space to separate class names -->
    <!-- <ul class="nav nav-pills" class="list-inline"> -->
    <ul>
            <!-- <li><a href="ModifyOrder.php">Modify Order</a></li>
              <li><a href="DeleteOrder.php">Delete Order</a></li> -->
              <li style="float:center"><a class="active" href="Checkout.php">Checkout</a></li>
              <li><a href="ProductDetails.php">Home</a></li>
    </ul>
    <button onclick="goBack()">Previous Page</button>
  </nav>
  
    <div class="col-25">
      <div class="container">
        <h4>Products Available:
          <span class="price" style="color:black">
            <i class="fa fa-shopping-cart"></i>
          </span>
        </h4>
        <img src="ProductImages/VTShirt.jpg" alt="VT Shirt" style="width:10%">
        <h3>VT Shirt</h3>
        <p class="price">Price: $5.97</p>
        <p>Virginia Tech Shirt. This shirt is part of our newest collection which we have been able to add into the store for your benefit.</p>
        <p><button>Add to Cart</button></p>
        <hr>
        </h4>
        <img src="ProductImages/VTSweats.jpg" alt="VT Sweats" style="width:10%">
        <h3>VT Sweatpants</h3>
        <p class="price">Price: $12.00</p>
        <p>Virginia Tech Sweatpants. These sweatpants is part of our newest collection which we have been able to add into the store for your benefit.</p>
        <p><button>Add to Cart</button></p>
        <hr>
        </h4>
        <img src="ProductImages/VTLegoSet.jpg" alt="VT Lego Set" style="width:10%">
        <h3>VT Lego Set</h3>
        <p class="price">Price: $7.45</p>
        <p>Virginia Tech Lego Set. This Lego Set is part of our newest collection which we have been able to add into the store for your benefit.</p>
        <p><button>Add to Cart</button></p>
        <hr>
        </h4>
        <img src="ProductImages/VTLoungeChair.jpg" alt="VT Lounge Chair" style="width:10%">
        <h3>VT Lounge Chair</h3>
        <p class="price">Price: $29.95</p>
        <p>Virginia Tech Lounge Chair. This chair is part of our newest collection which we have been able to add into the store for your benefit.</p>
        <p><button>Add to Cart</button></p>
        <hr>
        </h4>
        <img src="HokieBobbleHead.jpg" alt="VT Hokie BobbleHead" style="width:10%">
        <h3>Hokie Bobble Head</h3>
        <p class="price">Price: $20.00</p>
        <p>Virginia Tech Shirt. This Bobble Head is part of our newest collection which we have been able to add into the store for your benefit.</p>
        <p><button>Add to Cart</button></p>
        <hr>
        <p>Total <span class="price" style="color:black"><b>$30</b></span></p>
      </div>
    </div>
  </div>
</body>

</html>