
<?php
    $Email="";
    $Password ="";
    $timestamp;
    $remember = "no";
    $error = false;
    $loginOK = null;

    if (isset($_POST["submit"])) {
        if(isset($_POST["Email"])) $Email=$_POST["Email"];
        if(isset($_POST["CPassword"])) $Password=$_POST["CPassword"];
       // if(isset($_POST["remember"])) $remember=$_POST["remember"];
        date_default_timezone_set("America/New_York");
         // $hour = date("H");
         // $time = date("F j, Y,g:i a");
 
         if(empty($Email)||empty($Password)){
            $error = true;
        }
         
        if(!$error){

            require_once("db.php");
            $sql = "select CPassword from customers where Email = '$Email'";
            $result = $mydb->query($sql);
    
            $row = mysqli_fetch_array($result);
            if($row){
               if(strcmp($Password, $row["CPassword"])== 0){
                   $loginOK = true;
               } else {
                   $loginOK = false;
               }
            }
            if($loginOK){
                 session_start();
                 
                 $_SESSION["Email"] = $Email;
                 $_SESSION["CPassword"] = $Password;
                 setcookie("timestamp",date("F j, Y,g:i a"),time(),time()+60*60*24*2,"/");
                 date_default_timezone_set('America/New_York');
 
                 Header("HTTP/1.1 307 Temporary Redirect");
                 Header("Location:CustMainReal.html");
 
                 //set cookies to remeber timestamp after logging in
                 // date_default_timezone_set('US/Eastern');
                 // $date = date('Y-m-d h:i:sa', $_COOKIE['logtime']);
                 // setcookie("logtime",$_COOKIE['logtime'],time()+60*60*24*2,"/");
        
             }
         }
     }
?>

<!doctype html>
<html>
<head>
    <meta charset = "UTF-8">
    <meta name = "viewport" content="width=device-width,initial-scale=1.0">
    <meta name = "author" content="Supreet Pannu">
    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <script src="jquery-3.1.1.min.js"></script> 
    <script src="js/bootstrap.min.js"></script> 
    <title>Customer Login</title>
   

    <style>
        .errlabel {color:red}
        ul{
            list-style-type: none;
            margin: 0;
            padding: 0; 
            overflow: hidden;
            background-color: maroon; 
        }

        li {
            float: right;
        }

        li a{
            display: block;
            color:white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        }

        li a:hover{
            background-color: orange;
        }

        .active{
            background-color:orange;
        }


    </style>
<head>

<body>
           <!--Navigation bar-->
          <nav class="navbar navbar-light" style="background-color:#800000" role="navigation">
            <ul class="nav nav-pills">    
                <li><a href="HomePage.php">Home</a></li>
                <li><a href="Review.php">Review</a></li>
                <li><a href="AboutUs.html">About Us</a></li>
                <li><a href="ContactUs.php">Contact Us</a></li>
            </ul>
          </nav>
        </body>

      
    </br>
      </br>


<body>
    <h1 style="width:240px; margin: 0px auto; ">Customer Login</h1>
    <section>
    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']?>">
    <fieldset class="forminputs" style="width:500px; margin: 0px auto; ">
    <table>
            <tr>
                <td>Enter Email</td>
            </tr>
            <tr>
                <td><input type="text" name="Email" value="<?php if(!empty($Email)) echo $Email;?>" />
                    <?php if($error && empty($Email)) echo "<span class='errlabel'> Please enter your Email</span>"; ?> </td>
            </tr>
            <tr>
                <td>Enter Password</td>
            </tr>  
            <tr>
                <td><input type="password" type="hidden" name="CPassword" value="<?php if(!empty($Password)) echo $Password;?>" />
                    <?php if($error && empty($Password)) echo "<span class='errlabel'> Please enter a password</span>"; ?> </td>
            </tr> 
        
        </table>

       <!--  <table>
            <tr>
                <td><input type="checkbox" name="remember" value="yes"/><label>Remember me</label></td>
            </tr>
            <tr>
                <td><?php if(!is_null($loginOK) && $loginOK==false) echo "<span class='errlabel'>Customer username and password do not match.</span>"; ?></td>
            </tr>
            </table> -->
        <br />
        <br />
        <br />
    
    <input style="width:200px; margin: 0px auto; " type="submit" name="submit" value="Sign In" />
    <input style="width:200px; margin: 0px auto; " type="submit" name="forgot" value="Forgot Password" />

    <br />
    </fieldset>
    </section>
    </form> 
    
    <p> Don't have an account sign up <a href= "CustSignUp.php">Here</a></p>


    <?php include 'Footer.php'; ?>
    </body>
</html>