<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name= "viewport" content="width=device-width, initial-scale=1.0">
        <title>Product Information </title>

             <!-- ////javascript ajax Q1.3 -->
        <script src="jquery-3.1.1.min.js"></script>
        <script>
            function getProductInfo(){
                var x = document.getElementById("ContentArea");
                try {
                    asyncRequest = new XMLHttpRequest();

                    //register event handler
                    asyncRequest.onreadystatechange=stateChange;
                    var url="ProdDatabase.php";
                    asyncRequest.open('GET',url,true);
                    asyncRequest.send(null);
                } 
                catch (exception) {alert(exception);}
            } 

            function clearProductInfo(){
                document.getElementById("ContentArea").innerHTML = "";
            }

            function init(){
                document.getElementById("allProductsLink").addEventListener("mouseover", getProductInfo);
                document.getElementById("allProductsLink").addEventListener("mouseout", clearProductInfo);
            }

            document.addEventListener("DOMContentLoaded", init);

            function stateChange() {
                // if request completed successfully
                if(asyncRequest.readyState==4 && asyncRequest.status==200) {
                    document.getElementById("ContentArea").innerHTML=
                    asyncRequest.responseText;
                }
            }
                //jquery ajax
            //  $(document).ready(function(){
            //      $("#employeeDropDown").change(function(){
            //          var empID = $(this).val();
            //          $.ajax({
            //              url:"EmpDatabaseNew.php?empID=" + empID,
            //              async:true,
            //              success: function(result){
            //                  $("#ContentArea").html(result);
            //              }             
            //          });
            //      });
            //  });
        </script>
    </head>

    <body> 
        <!--Q1.1-->
        <medium><a id="allProductsLink" href="">All Products</a></medium> 
        <br/>
        <br />
            <div id="ContentArea">&nbsp;</div> 
    </body>
</html>