<?php
$email = "";
$InStockQuantity = "";
$phoneNumber = "";
$imgSource = "ProductImages/VTLegoSet.jpg";
$PID = "";
$PPrice = "";
$err = false;

//SQL SYNTAX HERE//

?>

<!DOCTYPE html>
<html lang="" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Virginia Tech Lego Set</title>
    <style>
      .errlabel     {color:red;}
      .center       {margin: auto;
                      width: 640px;
                      border: 3px solid black;
                      padding: 10px;}
      .centerText   {margin: auto;
                      width: 680px;
                      border: 3px solid black;
                      padding: 10px;}
      .centerImage  {display: block;
                      margin-left: auto;
                      margin-right: auto;
                      width: 500px;}
      .carousel     {width:900px;
                      height:630px;}
      .inputAlign   {margin-left: 1250px;
                      margin-top: -550px;
                      width: 400px;
                      border: 3px solid black;
                      padding: 10px;}
      .productInfo  {margin-left: 0px;
                      margin-top: 300px;
                      width: 900px;
                      border: 3px solid black;
                      padding: 10px;}
      .releaseTimer {margin-left: 1250px;
                      margin-top: 100px;
                      width: 400px;
                      border: 3px solid black;
                      padding: 10px;}
      .price        {margin-left: 980px;
                      margin-top: -173px;
                      width: 80px;
                      border: 3px solid black;
                      padding: 10px;}

    </style>
</head>

<body>
<nav>
    <!--Use a space to separate class names -->
    <!-- <ul class="nav nav-pills" class="list-inline"> -->
    <ul>
            <li><a href="AboutUs.html">AboutUs</a></li>
              <li><a href="Reviews.html">Review</a></li>
              <li style="float:center"><a class="active" href="Account.html">Account</a></li>
              <li><a href="HomePage.html">Home</a></li>
    </ul>
    <button onclick="goBack()">Previous Page</button>
  </nav>
  <br><br><br>
      <h1 class="centerText">Lane Stadium Lego Set</h1>
      <br><br><br>
      <div class="carousel">
        <div id="carousel1" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel1" data-slide-to="0" class="active"></li>
            <li data-target="#carousel1" data-slide-to="1"></li>
            <li data-target="#carousel1" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="ProductImages/VTLegoSet.jpg" width="100%" height="150px">
            </div>
            <div class="item">
              <img src="ProductImages/VTLegoSet2.jpg" width="100%" height="150px">
            </div>
            <div class="item">
              <img src="ProductImages/VTLegoSet3.jpg" width="100%" height="150px">
            </div>
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#carousel1" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel1" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="inputAlign">
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
      <table>
                  <td>Phone Number: </td>
                  <td><input type="text" name="phoneNumber">
                  <?php if($err && empty($phoneNumber)) echo "<br /><span class='errlabel'> Please enter a Phone Number.</span>"; ?>
                </td>
                    </select>
                  <?php if($err && empty($size)) echo "<br /><span class='errlabel'> Please enter a Size.</span>"; ?>
                </td>
                </tr>
                <tr>
                  <td>Carrier: </td>
                  <td><select name="carrier">
                        <option value="AT&T">AT&T</option>
                        <option value="Verizon">Verizon</option>
                        <option value="Sprint">Sprint</option>
                        <option value="TMobile">T-Mobile</option>
                    </select>
                  <?php if($err && empty($carrier)) echo "<br /><span class='errlabel'> Please enter a Carrier.</span>"; ?>
                </td>
                </tr>
                <tr></tr>
                <tr>
                  <td></td>
                  <td><input type="submit" name="submit" value="Purchase" /></td>
                </tr>
              </table>
        </form>
    </body>