 <?php
    $RID = 0;
    $PID = 0;
    $CID = 0;
    $payID=0;
    $Rating = 0;
    $ReviewText = "";
    $DateOfReview = "yyyy-MM-dd";
    $err = false;

    if (isset($_POST["submit"])) {
        if(isset($_POST["CID"])) $CID=$_POST["CID"];
        if(isset($_POST["RID"])) $RID=$_POST["RID"];
        if(isset($_POST["payment_id"])) $payID=$_POST["payment_id"];
        if(isset($_POST["PID"])) $PID=$_POST["PID"];
        if(isset($_POST["Rating"])) $Rating=$_POST["Rating"];
        if(isset($_POST["ReviewText"])) $ReviewText=$_POST["ReviewText"];
        if(isset($_POST["DateOfReview"])) $DateOfReview=$_POST["DateOfReview"];

        if($RID>0 && $PID>0 && $CID>0 && !empty($Rating) && !empty($ReviewText) && !empty($DateOfReview)) {
            header("HTTP/1.1 307 Temporary Redirect");
            header("Location:CustMainReal.html");
        } else {
            $err = true;
        }    
    require_once("db.php");
        
    if($RID==0) {
        $sql ="insert into bit4444group13.review (payment_id,CID, PID, Rating, ReviewText, DateOfReview) 
        values($payID, $CID, $PID, '$Rating', '$ReviewText' ,'$DateOfReview')";
        //echo $sql;
        $result=$mydb->query($sql);
        //$prodID = mysqli_insert_id();

        if($result==1){
            echo "<p>A new review record has been added</p>";
        }
    } 
}  

?> 




<!doctype html>
<html>
<head>
    <meta charset = "UTF-8">
    <meta name = "viewport" content="width=device-width,initial-scale=1.0">
    <meta name = "author" content="Supreet Pannu">
    
    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <script src="jquery-3.1.1.min.js"></script> 
    <script src="js/bootstrap.min.js"></script> 
    <title>Review this Product!</title>
   

    <style type="text/css">
       
       /* body {Background-color: lightblue;} */
       body {
            font-family: Arial, Helvetica, sans-serif;
        }

       /* .maroon{color: maroon;
            font-family: Arial Black;
            font-weight: 700;
            font-size: 19pt;
    
        }
        */
       .errlabel {color:red}

         /* ul{
            list-style-type: none;
            margin: 0;
            padding: 0; 
            overflow: hidden;
            background-color: maroon; 
        } */

        /* li {
            float: right;
        } */

         li a{
            display: block;
            color:white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        } 

        /* li a:hover{
            background-color:orange ;
        } */

        /* .active{
            background-color:orange;
        } */

        .active{
            background-color:orange;
        }

    </style>
<head>

<body>

<body>
           <!--Navigation bar-->
          <nav class="navbar navbar-light" style="background-color:#800000" role="navigation">
            <ul class="nav nav-pills">    
                <li style="float:center"><a href="HomePage.php">Home</a></li>
                <li class="active"><a href="Review.php">Review</a></li>
                <li><a href="AboutUs.html">About Us</a></li>
                <li><a href="ContactUs.php">Contact Us</a></li>
                <li><a href="EmpLogout.php">Log Out</a></li>
            </ul>
          </nav>
        </body>

      
    </br>
      </br>
</body>
        <br/>
        <br/>
        
        



    <body>
        <H1> Review a Product! </h1>
        <section>
        <form method="POST" action="<?php echo $_SERVER['PHP_SELF']?>">
        <fieldset style="width:500px; margin: 0px auto; ">

        <label>Review ID (Leave it blank for a new Review):</label>
         <input type="number" name="RID" maxlength="4" size="4" value="<?php echo $RID; ?>"/>
         
         <br />
         <br />
         <br />

         <label>Payment ID: </label>
         <input type="number" name="payment_id" maxlength="4" size="4" value="<?php echo $payID; ?>"/>
         <?php
            if($err && empty($payID)){
                echo "<label class='errlabel'>Error: Please enter the review</label>";
            }   
         ?>
         <br />
         <br />
         <br />


         <label>Customer ID:</label>
         <input type="number" name="CID" maxlength="4" size="4"value="<?php echo $CID; ?>"/>
         <?php
            if($err && empty($CID)){
                echo "<label class='errlabel'>Error: Please enter the review</label>";
            }   
         ?>
         <br />
         <br />
         <br />

         <label>Product ID:</label>
         <input type="number" name="PID" maxlength="4" size="4" value="<?php echo $PID; ?>"/>
         <?php
            if($err && empty($PID)){
                echo "<label class='errlabel'>Error: Please enter the review</label>";
            }   
         ?>
         <br />
         <br />
         <br />


        <label>Review Text:</label>
         <input name="ReviewText" type="text" size="50" required  value="<?php echo $ReviewText; ?>"/>
         <?php
            if($err && empty($ReviewText)){
                echo "<label class='errlabel'>Error: Please enter the review</label>";
            }   
         ?>
        <br />
        <br />
        <br />


        <label>Date:</label>
         <input name="DateOfReview" type="date" required  value="<?php echo $DateOfReview; ?>"/>
         <?php
            if($err && empty($DateOfReview)){
                echo "<label class='errlabel'>Error: Please enter the date</label>";
            }   
         ?>
        <br />
        <br />
        <br />


        <label>Rating: (1 to 10)</label>
        <input name="Rating" type="text" min="1" max="10" value="5"  required  value="<?php echo $Rating; ?>"/>
         <?php
            if($err && empty($Rating)){
                echo "<label class='errlabel'>Error: Please enter the rating</label>";
            }   
         ?>
        <br />
        <br />
        <br />

        <input style="width:200px; margin: 0px auto; " type="submit" name="submit" value="Submit"  />
    <!--onclick="submit_form();-->
        <input style="width:200px; margin: 0px auto; " type="reset"  name="refresh" value="Clear" />
        <br />
        <br />
        <br />
        </form>
        <br />
        <br />
        <br />
        
        </section>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <?php include 'Footer.php'; ?>
        <br />
        <br />
        <br />
    </body>

</html>