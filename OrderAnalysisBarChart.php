
        <!---D3 SECTION-->

<?php
  $custid=0;
  if(isset($_POST["submit"])){
    if(isset($_POST["CID"])) $custid=$_POST["CID"];
  }
  
?>

<!DOCTYPE html>
<html>
<head>
<!-- <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script> -->
  <title>Bar Chart</title>
<meta charset="utf-8">
<style>

.bar {
  fill: orange;
}

.bar:hover {
  fill: maroon;
}

.axis--x path {
  display: none;
}

li a{
            display: block;
            color:white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        } 

</style>
</head>

<svg width="960" height="500"></svg>
<script src="https://d3js.org/d3.v4.min.js"></script>
<script>

var svg = d3.select("svg"),
    margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom;

var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
    y = d3.scaleLinear().rangeRound([height, 0]);
    //console.log(y(3));

var g = svg.append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

//specify our data source
/*
d3.tsv("data.tsv", function(d) {
  d.frequency = +d.frequency;
  return d;
}, function(error, data) {
*/

//the selected orderid is passed here
//the same id must be passed to getData.php
<?php
  $custid=0;
  if(isset($_GET["CID"])) $custid=$_GET["CID"];
  $custid = $_POST["CID"];
?>

d3.json("getOrderData.php?CID=" + <?php echo $custid?>, function(error, data){
  if(error) throw error;

  // data.forEach(function(d){
  //   d.letter = d.OID;
  //   d.frequency = +d.TotalValue;

    data.forEach(function(d){
    d.letter = d.DateOfPurchase;
    d.frequency = +d.TotalValue;
  })

  console.log(data);

  if (error) throw error;

  x.domain(data.map(function(d) { return d.letter; }));
  y.domain([0, d3.max(data, function(d) { return d.frequency; })]);

  g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));

  g.append("g")
      .attr("class", "axis axis--y")
      .call(d3.axisLeft(y).ticks(10, "s"))
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("Frequency");

  g.selectAll(".bar")
    .data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.letter); })
      .attr("y", function(d) { return y(d.frequency); })
      .attr("width", x.bandwidth())
      .attr("height", function(d) { return height - y(d.frequency); });
});

</script>

</br>



<p>
    <a href="Orders.php">Return to Orders</a>
</p>
</body>


</html>