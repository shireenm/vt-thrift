<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>

.fa {
  padding: 20px;
  font-size: 30px;
  width: 50px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fContent {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: #f2f2f2;
   color: black;
   text-align: center;
}

* {
  box-sizing: border-box;
}

/* Create four equal columns that floats next to each other */
.column {
  float: left;
  width: 25%;
  padding: 10px;
  /* height: 150px; Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>
</head>

<body>
    <div class="fContent">

        <div id="footer" class="section">

            <div class="column one">
                <h3><strong>Contact US</strong></h3>
                    Phone: 540-852-1980
                    <br/>
                    Email: VTThrift@gmail.com
                    
            </div>

            <div class="column two">
                <h3><strong>Address</strong></h3>
                1234 North Main St. Blacksburg, VA 24060
            </div>

            <div class="column three">
                <h3><strong>Quick Links</strong></h3>
                <a href="HomePage.html">Home</a>
                <br/>
                <a href="AboutUs.html">About</a>
                <br/>
                <a href="CustSignUp.php">Sign Up</a>
            </div>
            

            <div class="column four">
                <h3><strong>Follow Us</strong></h3>
                <a href="http://www.facebook.com/VTthrift" class="fa fa-facebook"></a></a>
                
                <a href="http://www.instagram.com/VTthrift" class="fa fa-instagram"></a></a>
                
                <a href="http://www.twittercom/VTthrift" class="fa fa-twitter"></a></a>
            </div>
            <br/>
            <br/>
            <br/>
            
            <medium><div class="center black-text">&copy; 2020 VTThrift</div></medium>

        </div>

    </div>

        

</body>

</html>