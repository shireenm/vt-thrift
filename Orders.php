<?php
    
    // $conn=mysqli_real_connect("pamplin-bit2020.mysql.database.azure.com", "bit4444group13", "GtMEKzZrhhTAOsF7", "bit4444group13");

    // //require_once("db.php");
    // $query = "select OID, CID, DateOfPurchase, TotalCosts, PaymentStatus from bit4444group13.orders where PaymentStatus=0";
    // $sql = mysqli_query($conn,$query);

    require_once("db.php");
    $sql="select OID, CID, DateOfPurchase, TotalCosts, PaymentStatus from bit4444group13.orders where PaymentStatus=1 ";
    $result=$mydb->query($sql);



?>


<!doctype html>
<html>
<head>
    <meta charset = "UTF-8">
    <meta name = "viewport" content="width=device-width,initial-scale=1.0">
    <!-- <link href="css/bootstrap.min.css"> -->
    <title>Orders</title>
    <script src="jquery-3.1.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css"/>
    <!-- <script src="jquery-ui.css"></script>
    <script src="jquery.dataTables.min.css"></script>
    <script src="jquery-ui.js"></script>
    <script src="jquery.dataTables.min.js"></script> -->


    <!--<script src="js/bootstrap.min.js"></script> -->

    <style>
            .orange {color:orange;}
            .maroon{color: maroon;}
            .blue {color:blue;}
    </style>

<!-- <script src="jquery-3.1.1.min.js"></script>  -->


    <script>
        //this whole script uses jQuery to display countdown til HokieBrd picture day
                  
            $(function (){
                setInterval(updateTime, 1000);
                $("#hokiebird").click(function(){
                    if($("#hokiebird").hasClass("orange")){
                        $("#hokiebird").removeClass("orange").addClass("maroon");
                    } 
                    else if($("#hokiebird").hasClass("maroon")){
                        $("#hokiebird").removeClass("maroon").addClass("purple");
                    } 
                    else if($("#hokiebird").hasClass("purple")){
                        $("#hokiebird").removeClass("purple").addClass("orange");
                    }
                    else{
                        $("#hokiebird").addClass("orange");
                    }
               
                })

                 //user double clicks, clock fades out

                 $("#hokiebird").dblclick(function() {
                    $("#hokiebird").fadeOut();
                }) 

                // $("#Vote").hover(function()
                // {this.src="ballotBox.jpg"}).mouseleave(function(){this.src="voteFlag.jpg"});  
            })

        
            function updateTime(){
                var hokieDate = new Date("12/07/2020 02:00:00 EST +01:00").getTime();
                var today = new Date().getTime();

                var diff = hokieDate - today;  


            // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(diff / ( 1000 * 60 * 60 * 24));
                var hours = Math.floor((diff % ( 1000 * 60 * 60 * 24)) / ( 1000 * 60 * 60));
                var minutes = Math.floor((diff % ( 1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((diff % (1000 * 60)) / 1000);
                  
                $("#hokiebird").text("There are " + days + " days, " + hours + " hours, " + minutes + " minutes, and "  + seconds + " seconds until VT-THRIFT Grand Opening!!" );

                }



        </script>

    </head>


    <body>
        <p id="hokiebird"></p> 

    </body>

    <style type="text/css">

        /* body {Background-color: lightblue;} */

        .maroon{color: maroon;
            font-family: Arial Black;
            font-weight: 700;
            font-size: 19pt;

        }

        .errlabel {color:red}
       /* ul{
            list-style-type: none;
            margin: 0;
            padding: 0; 
            overflow: hidden;
            background-color: maroon; 
        } */

        /* li {
            float: right;
        } */

        li a{
            display: block;
            color:white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        } 

        /* li a:hover{
            background-color:orange ;
        } */

        /* .active{
            background-color:orange;
        } */


    </style>
<head>



<!-- <body>
<nav> -->
    <!--Use a space to separate class names -->
    <!-- <ul class="nav nav-pills" class="list-inline"> -->
    <!-- <ul>
    <li><a href="OrderLogOut.php">Log Out</a></li>  
      <li><a href="ManMain.html">Main Page</a></li>
      <li style="float:center"><a class="active" href="Orders.php">Orders</a></li>

    </ul> -->
  <!-- </nav>
</body> -->

<body>
           <!--Navigation bar-->
          <nav class="navbar navbar-light" style="background-color:#800000" role="navigation">
            <ul class="nav nav-pills">    
                <li><a href="OrderLogOut.php">Log Out</a></li>
                <li><a href="ManMain.html">Main Page</a></li>
                <li style="float:center"><a class="active" href="Orders.php">Orders</a></li>
          </nav>
        </body>

      
    </br>
      </br>
<br/>
<br/>
<br/>
<br/>
<br/>


<body>
<br/>

<div class="container">

<br/>
<br/>

<div class="col-md-2">
<input type="text" name="start" id="start" class="form-control" placeholder="From Date"/>
</div>

<div class="col-md-2">
<input type="text" name="end" id="end" class="form-control" placeholder="To Date"/>
</div>

<div class="col-md-8">
<input type="button" name="range" id="range" value="Range" class="btn btn-success" class="form-control"/>
</div>

<div class="clearfix"></div>

<br/>

<div id="orderStuff">
<table class="table table-bordered">
    <tr>
        <th>Order ID</th>
        <th>Customer ID</th>
        <th>Date of Purchase</th>
        <th>Total Costs</th>
        <th>Open Orders</th>
    </tr>


<?php
while($row= mysqli_fetch_array($result))
{
    ?>
        <tr>
            <td><?php echo $row["OID"]; ?></td>
            <td><?php echo $row["CID"]; ?></td>
            <td><?php echo $row["DateOfPurchase"]; ?></td>
            <td><?php echo $row["TotalCosts"]; ?></td>
            <td><?php echo $row["PaymentStatus"]; ?></td>
        </tr>
    <?php
}
?>

</table>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<!-- Script -->
<script>
    
    $(function(){
        $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd'
    });
        $("#start").datepicker();
        $("#end").datepicker();

    $('#range').click(function(){

        var From = $('#start').val();

        var to = $('#end').val();
        console.log(From);
        if(From != '' && to != '')
        {
            $.ajax({
                url:"OrderDateRange.php",
                method:"POST",
                data:{start:From, end:to},
                success:function(data)
                {
                    $('#orderStuff').html(data);
                }
            });
        }
        else
        {
            alert("Please Select the Date");
        }
    });
});
</script>
<button onclick="window.location.href = 'OrderAnalysisForm.html';" type="button" id="button4">Order Analysis Form</button>
</br>
</br>
<button onclick="window.location.href = 'orderIndex.php';" type="button" id="button5">Analyze</button>

</body>
</html>

