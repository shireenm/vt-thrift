<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>How it Works</title>
    <style media="screen">
     .centerText       {margin: auto;
                        width: 240px;
                        border: 3px solid black;
                        padding: 10px;}
    /* .centerImage      {display: block;
                        margin-left: auto;
                        margin-right: auto;
                        width: 500px;}
    .center           {margin: auto;
                        width: 640px;
                        border: 3px solid black;
                        padding: 10px;} */
    .centerHowItWorks {margin: auto;
                        width: 660px;
                        border: 3px solid black;
                        padding: 10px;} 
                        .errlabel {color:red}
        ul{
            list-style-type: none;
            margin: 0;
            padding: 0; 
            overflow: hidden;
            background-color: maroon; 
        }

        li {
            float: right;
        }

        li a{
            display: block;
            color:white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        }

        li a:hover{
            background-color: orange;
        }

        .active{
            background-color:orange;
        }

        .clothing-img {
          width: 100vw;
        }
    </style>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <!-- <a href="Homepage.php"><img src="VTThrift logo.jpg" class='centerImage'></a> -->
      <nav class='center'>
      <ul class="nav nav-pills">
            <li><a href="HomePage.php">Home</a></li>
            <li><a href="ProductDetails.php">Shop</a></li>
            <li><a href="HowToPage.php">How it Works</a></li>
            <li><a href="ModifyOrder.php">Modify Order</a></li>
            <li><a href="VTThriftSuccess.php">Previous Success</a></li>
            <li><a href="EmployeeLogin.php">Admin Login</a></li>
              </ul>
            </li>
          </ul>
        </nav>
        <br><br><br>
      <h1 class="centerText">How it Works</h1>
    </div>
    <br><br><br>
    <div class="centerHowItWorks">
      <ol>
        <li>
          The first step is to select the item of choice you would like us to secure on our website, then checkout.
          After completing the checkout process you will receive an order number and confirmation email from us,
          this ensures everything has been completed correctly.
        </li>
        <li>
          You will be asked to fill out some information on the product page of your desired item, before you checkout.
          That information will be used to successfully secure the item you selected from us on the day of release.
          It is very important the information you provide to us is correct, otherwise your order will not be submitted correctly.
        </li>
        <li>
          Lastly you will be brought to the checkout process of our site where you will pay for the slot (our service fee).
        </li>
        <li>
          Once you complete the order process an authorization will be placed on your credit card for the amount the slots are.
          After we successfully secure an item after the release is over, we will process and submit the charge on your credit card.
          If for any reason we are unable to secure an item selected the authorization will be dropped from your credit card account,
          and you will not be charged.
        </li>
        <li>
          On the day of release you will receive a confirmation from the actual site we are making the purchase from, notifying you
          that your order has been successfully placed.
        </li>
        <li>
          Once again if for any reason we are unable to fulfill your order the authorization placed on your account will be dropped
          immediately, and you will not be charged for the service. If your orders are successfully fulfilled you will be
          charged 24 hours after the release date.
        </li>
      </ol>
    </div>
    <br>
    <p align='center'>&copy;<small>2020 VT Thrift , Inc. All Rights Reserved.</small></p>
    <br>
  </body>
</html>