<!doctype html>
<html>
<head>
    <meta charset = "UTF-8">
    <meta name = "viewport" content="width=device-width,initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
    <title>Product Analysis Form</title>
   

    <style>
        .headers{background-color: orange; color: white}
        /* .product{background-color:orange; color:black}; */
        .contents{background-color:white; color:black}
 
        body {
            font-family: Arial, Helvetica, sans-serif;
            }
        .maroon{color: maroon;
             font-family: Arial black;
             font-weight: 700;
             font-size: 19pt;
     
         }
        
        .errlabel {color:red}
        

        li a{
            display: block;
            color:white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        } 

    </style>
</head>

<body>
           <!--Navigation bar-->
          <nav class="navbar navbar-light" style="background-color:#800000" role="navigation">
            <ul class="nav nav-pills">    
                <li><a href="HomePage.php">Home</a></li>
                <li><a href="Review.php">Review</a></li>
                <li><a href="AboutUs.html">About Us</a></li>
                <li><a href="ContactUs.php">Contact Us</a></li>
                <li><a href="EmpLogout.php">Log Out</a></li>
            </ul>
          </nav>
        </body>

      
    </br>
      </br>
<section>
    <body>
        <h1 class = "maroon">Product Analysis Form</h1>
        <p>
            Please fill out this form to complete a Product analysis.
        </p>
    <form method="POST" action="" autocomplete="on">
        <p>
            <label>Product ID:
                <input type="number" name="ManID" size="2" required autofocus/>
            </label>
        </p>

        <p>
            <label>Today's Date:
                <input type="date" name="today" size="2" required autofocus/>
            </label>
        </p>

        
        <p>
            <label>What week or month is this product form for? Please write it out in the textbox below</label>
            <br/>
            <textarea name="dates" rows="3" cols="30" placeholder="The Week of 06/23/2020  OR The month of June 2020"></textarea>
        </p>
        
        <p>
            <label>List the products that need to be re-stocked</label>
            <br/>
            <textarea name="re-stock" rows="6" cols="10" placeholder="Product 256"></textarea>
        </p>

        <p>
            <label>Which product was purchased the most during this time period</label>
            <br/>
            <textarea name="sales" rows="3" cols="10" placeholder="Product 256"></textarea>
        </p>

    
        <p>
            <label>What was the most expensive product bought recently:
                <input type="text" id="expensive" name="eproduct"  required autofocus/>
            </label>
        </p>

    
    
</section>
<p>
    <input type="submit" value="Submit" />
    <input type="reset"  value="Clear" />
</p>    
</form> 
</body>
<p>
    <a href="Product.php">Return to Prodcuts</a>
</p>
</html>