<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Supreet Pannu">
    <title>Review index</title>
</head>
<body>
    <p>Select a review Index to see this products average</p>
    <form method="post" action="ReviewAnalysisBarChart.php">
        <select name="RID">
            <?php
                require_once("db.php"); 
                $sql = "select distinct RID from bit4444group13.review";
                $result=$mydb->query($sql);
                echo "<a> All Reviews </a>";
                while($row = mysqli_fetch_array($result)){
                    $val = $row['RID'];
                    echo "<option value='$val'>".$val."</option>";
                };
            ?>
 
            <!--<option value="1">1</option>-->
        </select>
 
        <input type="submit" name="submit" value='Submit'>
 
    </form>
 
</body>
</html>