<?php
// $customer_username = "";
// $Cpassword = "";
$OID = "0";
// $orderEmail = "";
// $orderPassword = "";
// $phoneNumber = "";
$err = false;

if (isset($_POST["Delete Order"])) {
    // if(isset($_POST["customer_username"])) $customer_username=$_POST["customer_username"];
    // if(isset($_POST["Cpassword"])) $Cpassword=$_POST["Cpassword"];
    if(isset($_POST["OID"])) $OID=$_POST["OID"];
    // if(isset($_POST["orderEmail"])) $orderEmail=$_POST["orderEmail"];
    // if(isset($_POST["orderPassword"])) $orderPassword=$_POST["orderPassword"];
    // if(isset($_POST["size"])) $size=$_POST["size"];
    // if(isset($_POST["phoneNumber"])) $phoneNumber=$_POST["phoneNumber"];
    if ($OID>0) {require_once ("db.php");
      if($OID>0) {
        //header("HTTP/1.1 307 Temporary Redirect");
        //header("Location:EmpDatabaseNew.php");

        require_once("db.php");

        if($OID>0) {
            $sql ="delete from orders where OID = $OID";
            //echo $sql;
            $result=$mydb->query($sql);
        
            if($result==1){
                echo "<p>An orders record has been deleted</p>";
            }else{
                echo "<p>An orders record was not deleted</p>";
            }
        }
    
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Delete Order Confirmation</title>
    <style media="screen">
    .errlabel     {color:red;}
    .centerImage      {display: block;
                        margin-left: auto;
                        margin-right: auto;
                        width: 500px;}
    .center           {margin: auto;
                        width: 640px;
                        border: 3px solid black;
                        padding: 10px;}
    .inputAlign       {margin: auto;
                        width: 370px;
                        border: 3px solid black;
                        padding: 10px;}
    .centerText       {margin: auto;
                        width: 450px;
                        border: 3px solid black;
                        padding: 10px;}
    .confirmationText {margin: auto;
                        width: 660px;
                        border: 3px solid black;
                        padding: 10px;}
    </style>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <a href="HomePage.php"><img src="VTThriftLogo.jpg" class='centerImage'></a>
      <nav class='center'>
          <ul class="nav nav-pills">
          <ul class="nav nav-pills">
            <li><a href="HomePage.php">Home</a></li>
            <li><a href="ProductDetailsIndex.php">Shop</a></li>
            <li><a href="HowToPage.php">How it Works</a></li>
            <li><a href="ModifyOrder.php">Modify Order</a></li>
            <li><a href="VTThriftSuccess.php">Previous Success</a></li>
            <li><a href="EmployeeLogin.php">Admin Login</a></li>
              </ul>
            </li>
          </ul>
        </nav>
        <br><br><br>
      <h1 class="centerText">Delete Order Confirmation</h1>
      <br><br><br>
      <?php
          echo "<p class='confirmationText'>
                  Congradulations! You have successfully deleted your order, order number $OID.
                  You will be receiving a refund to your credit card in 3-5 business days.
                </p>";
      ?>
    </div>
    <br>
    <p align='center'>&copy;<small>2020 VT Thrift, Inc. All Rights Reserved.</small></p>
    <br>
  </body>
</html>