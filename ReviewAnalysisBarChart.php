
        <!---D3 SECTION-->

        <?php
  $RID=0;
  if(isset($_POST["submit"])){
    if(isset($_POST["RID"])) $RID=$_POST["RID"];
  }
  
?>

<!DOCTYPE html>
<html>
<head>
  <title>Review Bar Chart</title>
<meta charset="utf-8">
<meta name = "author" content = "Supreet Pannu">
<style>

.bar {
  fill: orange;
}

.bar:hover {
  fill: green ;
}

.axis--x path {
  display: none;
}

</style>
</head>
<body>
<svg width="960" height="500"></svg>
<script src="https://d3js.org/d3.v4.min.js"></script>
<script>

var svg = d3.select("svg"),
    margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom;

var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
    y = d3.scaleLinear().rangeRound([height, 0]);
    //console.log(y(3));

var g = svg.append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

//specify our data source
/*
d3.tsv("data.tsv", function(d) {
  d.frequency = +d.frequency;
  return d;
}, function(error, data) {
*/

//the selected supplierid is passed here
//the same id must be passed to getData.php
<?php
  $RID=0;
  if(isset($_GET["RID"])) $RID=$_GET["RID"];
  $RID = $_POST["RID"];
?>

d3.json("GetReviewData.php?RID=" + <?php echo $RID?>, function(error, data){
  if(error) throw error;

  data.forEach(function(d){
    d.letter = d.PID;
    d.frequency = +d.Rating;
  })

  console.log(data);

  if (error) throw error;

  x.domain(data.map(function(d) { return d.letter; }));
  y.domain([1, d3.max(data, function(d) { return d.frequency; })]);

  g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(1," + height + ")")
      .call(d3.axisBottom(x));

  g.append("g")
      .attr("class", "axis axis--y")
      .call(d3.axisLeft(y).ticks(10, "s"))
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("Frequency");

  g.selectAll(".bar")
    .data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.letter); })
      .attr("y", function(d) { return y(d.frequency); })
      .attr("width", x.bandwidth())
      .attr("height", function(d) { return height - y(d.frequency); });
});

</script>

<a href= "HomePage.php"><p>Click Here to go back to the home Page</a></p>
</body>