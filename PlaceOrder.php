<?php
  session_start();
  $OID = $_SESSION['OID'];
  $CCLName = $_SESSION['CCLName'];
  $CCNumber = $_SESSION['CCNumber'];
  $CCSecurityCode = $_SESSION['CCSecurityCode'];
  $CCExp = $_SESSION['CCExp'];
  $prodID = $_SESSION['prodID'];
  $customerId = $_SESSION['customerID'];
  $customerEmail = $_SESSION["customerEmail"];
  $phoneNumber = $_SESSION['phoneNumber'];
  $carrier = $_SESSION['carrier'];
  $size = $_SESSION['size'];
  $prodID = $_SESSION['prodID'];
  $price = $_SESSION['price'];


  require_once("db.php");
  $sql = "INSERT INTO payment(payment_cc_first_name, payment_cc_last_name, payment_cc_number, payment_cc_security_code, payment_cc_exp_date)
            VALUES('$CCFName', '$CCLName', $CCNumber, '$CCSecurityCode', '$CCExp')";
  $result=$mydb->query($sql);
  $paymentID = mysqli_insert_id($mydb->dbConn);

  $sql = "INSERT INTO bit4444group13.orders(CID,DateOfPurchase,TotalPreTaxCosts,TotalTax,TotalCosts,TotalQuantity,PaymentStatus)
            VALUES('$paymentID', '$customerId', '$prodID','$price')";
  $result=$mydb->query($sql);
  $orderID = mysqli_insert_id($mydb->dbConn);

  switch ($carrier) {
    case 'AT&T':
      $textAlertAddress = $phoneNumber."@txt.att.net";
      break;
    case 'Verizon':
      $textAlertAddress = $phoneNumber."@vtext.com";
      break;
    case 'Sprint':
      $textAlertAddress = $phoneNumber."@messaging.sprintpcs.com";
      break;
    case 'TMobile':
      $textAlertAddress = $phoneNumber."@tmomail.net";
      break;

    default:
      break;
  }


  use PHPMailer\PHPMailer\PHPMailer;

  require_once('PHPMailer/src/PHPMailer.php');
  require_once('PHPMailer/src/SMTP.php');
  require_once('PHPMailer/src/Exception.php');

  $mail = new PHPMailer();
  $mail->isSMTP();
  $mail->SMTPAuth = true;
  $mail->SMTPSecure = 'ssl';
  $mail->Host = 'smtp.gmail.com';
  $mail->Port = '465';
  $mail->isHTML();
  $mail->Username = 'pm6345@gmail.com';
  $mail->Password = 'biggerthanme123';
  $mail->SetFrom('no-reply@VT Thrift.com', 'VT Thrift');
  $mail->Subject = 'Order Confirmation';
  $mail->Body = "Thank you for your order, order number ".$OID.". You will
                  be receiving text and email alerts for this order.";
  $mail->AddAddress($textAlertAddress);
  $mail->AddAddress($Email);
  $mail->send();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Order Confirmation</title>
    <style>
    .center           {margin: auto;
                        width: 640px;
                        border: 3px solid black;
                        padding: 10px;}
    .centerText       {margin: auto;
                        width: 340px;
                        border: 3px solid black;
                        padding: 10px;}
    .centerImage      {display: block;
                        margin-left: auto;
                        margin-right: auto;
                        width: 500px;}
    .confirmationText  {margin: auto;
                        width: 50%;
                        border: 3px solid black;
                        padding: 10px;}

    </style>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <a href="Homepage.php"><img src="VTThrift Home Logo.png" class='centerImage'></a>
      <nav class='center'>
          <ul class="nav nav-pills">
          <li><a href="HomePage.php">Home</a></li>
            <li><a href="ProductDetails.php">Shop</a></li>
            <li><a href="HowToPage.php">How it Works</a></li>
            <li><a href="ModifyOrder.php">Modify Order</a></li>
            <li><a href="VTThriftSuccess.php">Previous Success</a></li>
            <li><a href="EmployeeLogin.php">Admin Login</a></li>
              </ul>
            </li>
          </ul>
        </nav>
        <br><br><br>
      <h1 class="centerText">Order Confirmation</h1>
      <br><br><br>
      <?php
        echo "<p class='confirmationText'>
                Congradulations! You have successfully placed your order. Your order number is $OID.
                A copy of your order confirmation has been sent to $Email and $phoneNumber.
                Please keep this number as you will need it to modify/delete your order.
                We are looking forward to seeing you soon!
              </p>";
      ?>
    </div>
    <br>
    <p align='center'>&copy;<small>2020 VT Thrift, Inc. All Rights Reserved.</small></p>
    <br>
  </body>
</html>