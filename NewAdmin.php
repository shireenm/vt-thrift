<DOCTYPE! html>
<?php
  $username = "";
  $pword = "";
  $error = false;
  $success = false;
  $exists = false;

  if (isset($_POST["submit"])) {
      if(isset($_POST["username"])) $username=$_POST["username"];
      if(isset($_POST["pword"])) $pword=$_POST["pword"];

      if(!empty($username) && !empty($pword)) {
        require_once("db.php");
        $sql = "Select * FROM admin WHERE admin_username = '$username'";
        $result=$mydb->query($sql);
        $row = mysqli_fetch_array($result);
        if ($row["admin_username"] == $username){
          $exists = true;
        }
        else {
          $sql = "insert into admin(admin_username, admin_password) values('$username', '$pword')";
          $result=$mydb->query($sql);
          if ($result==1) {
            $success = true;
            $username = "";
            $pword = "";
          }
        }
      } else {
        $error = true;
      }
  }
 ?>
<html>
  <head>
    <title>VT Thrift Add New Admin Page</title>
    <meta charset="utf-8">
    <meta name="author:" content="Jahmal Auguste">
    <meta name="description:" content="VT Theift must be able to Add new admins when needed.">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="jquery-3.1.1.min.js">
    </script>
    <script src="js/bootstrap.min.js">
    </script>
    <style>
      .errlabel {color:red;}
      .success {color:green;}
      .centerImage{display: block;
                  margin-left: auto;
                  margin-right: auto;
                  width: 500px;}

      .center     {margin:auto;
                  width: 500px;
                  border: 3px solid black;
                  padding: 10px;}

      .centerText {margin:auto;
                  width: 500px;
                  border: 3px solid black;
                  padding: 10px;}
    </style>
  </head>

  <body>
    <div class="container-fluid">
      <a href="HomePage.php"><img src="VTThrift Logo.jpg" class='centerImage'></a>
      <nav class="centerText">
        <ul class="nav nav-pills nav-justified">
            <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown">Information Tables <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="CustomerInfo.php">Customer Information</a></li>
              <li><a href="OrderInfo.php">Order Information</a></li>
              <li><a href="ProductInfo.php">Product Information</a></li>
              <li><a href="PaymentInfo.php">Payment Information</a></li>
            </ul>
          </li>
          <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown">Manage Products <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="addProduct.php">Add Product</a></li>
            <li><a href="deleteProduct.php">Remove Product</a></li>
          </ul>
        </li>
        <li class="dropdown active">
        <a class="dropdown-toggle" data-toggle="dropdown">Manage Admins <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="changePassword.php">Change Password</a></li>
          <li class="active"><a href="NewAdmin.php">Add New Admin To DB</a></li>
        </ul>
      </li>
          <li><a href="graphs.php">Graphs</a></li>
        </ul>
      </nav>
      <br />
      <h1 class="centerText">VT Thrift Add New Admin</h1>
      <br />
      <nav class="centerText">
        <p><strong>Enter username and password for account you wish to add: </strong></p>
        <br />
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
          <label>Username: </label>
          <input type="text" name="username" value="<?php echo $username; ?>"/>
          <?php
            if ($error && empty($username)) {
              echo "<label class='errlabel'>Error: Please enter a username</label>";
            }
          ?>
          <br />
          <br />
          <label>Password: </label>
          <input name="pword" type="password" value="<?php echo $pword; ?>"/>
          <?php
            if ($error && empty($pword)) {
              echo "<label class='errlabel'>Error: Please enter a password</label>";
            }
          ?>
          <br />
          <br />
          <input type="submit" name="submit" value="Submit" />
          <br />
          <br />
          <p>
            <?php if(!$error && $success && isset($username) && isset($pword))
            {
             echo "<span class='success'>New Admin successfully added. </span>";
           }
           elseif ($exists) {
            echo "<span class='errlabel'>Admin Username already exists. Please enter another username you wish to add.</span>";
           }
            ?>
          </p>
        </form>
      </nav>
      <br />
      <br />
    <p>Copyright &copy; 2020 VT Thrift, USA, all rights reserved</p>
    </div>
  </body>
</html>