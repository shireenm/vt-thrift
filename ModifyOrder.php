<?php
$customerUsername = "";
$customerPassword = "";
$OID = "";
$orderEmail = "";
$orderPassword = "";
$size = "";
$phoneNumber = "";

if (isset($_POST["submitOrderMod"])) {
    if(isset($_POST["customerUsername"])) $customerUsername=$_POST["customerUsername"];
    if(isset($_POST["customerPassword"])) $customerPassword=$_POST["customerPassword"];
    if(isset($_POST["OID"])) $orderNumber=$_POST["OID"];
    if(isset($_POST["orderEmail"])) $orderEmail=$_POST["orderEmail"];
    if(isset($_POST["orderPassword"])) $orderPassword=$_POST["orderPassword"];
    if(isset($_POST["size"])) $size=$_POST["size"];
    if(isset($_POST["phoneNumber"])) $phoneNumber=$_POST["phoneNumber"];
  }

  require_once("db.php");
  $sql = "UPDATE bit4444group13.orders SET orders.order_Email = '$orderEmail', orders.order_password = '$orderPassword', orders.order_shoe_size = '$size', orders.order_phone_number = '$phoneNumber'
	         WHERE orders.order_id = '$orderNumber';";
  $result = $mydb->query($sql);
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Modify Order Confirmation</title>
    <style media="screen">
    .errlabel     {color:red;}
    .centerImage      {display: block;
                        margin-left: auto;
                        margin-right: auto;
                        width: 500px;}
    .center           {margin: auto;
                        width: 640px;
                        border: 3px solid black;
                        padding: 10px;}
    .inputAlign       {margin: auto;
                        width: 370px;
                        border: 3px solid black;
                        padding: 10px;}
    .centerText       {margin: auto;
                        width: 460px;
                        border: 3px solid black;
                        padding: 10px;}
    .confirmationText {margin: auto;
                        width: 660px;
                        border: 3px solid black;
                        padding: 10px;}
    </style>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <a href="Homepage.php"><img src="VTThrift.jpg" class='centerImage'></a>
      <nav class='center'>
          <ul class="nav nav-pills">
            <li><a href="HomePage.php">Home</a></li>
            <li><a href="ProductDetails.php">Shop</a></li>
            <li><a href="HowToPage.php">How it Works</a></li>
            <li><a href="ModifyOrder.php">Modify Order</a></li>
            <li><a href="VTThriftSuccess.php">Previous Success</a></li>
            <li><a href="EmployeeLogin.php">Admin Login</a></li>
              </ul>
            </li>
          </ul>
        </nav>
        <br><br><br>
      <h1 class="centerText">Modify Order Confirmation</h1>
      <br><br><br>
      <?php
          echo "<p class='confirmationText'>
                  Congradulations! You have successfully modified your order.
                </p>";
      ?>
    </div>
    <br>
    <p align='center'>&copy;<small>2020 VT Thrift, Inc. All Rights Reserved.</small></p>
    <br>
  </body>
</html>