<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name= "viewport" content="width=device-width, initial-scale=1.0">
        <title>Employee Information </title>

             <!-- ////javascript ajax Q1.3 -->
             <!-- <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script> -->

        <style type="text/css">
       
        /* body {Background-color: lightblue;} */
 
        body {
            font-family: Arial, Helvetica, sans-serif;
            }
        .maroon{color: maroon;
             font-family: Arial black;
             font-weight: 700;
             font-size: 19pt;
     
         }
        
        .errlabel {color:red}
         /* ul{
            list-style-type: none;
            margin: 0;
            padding: 0; 
            overflow: hidden;
            background-color: maroon; 
        } */

        /* li {
            float: right;
        } */

        li a{
            display: block;
            color:white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        } 

        /* li a:hover{
            background-color:orange ;
        } */

        /* .active{
            background-color:orange;
        } */
 
     </style>
        <script>
            function getEmployeeInfo(){
                var x = document.getElementById("ContentArea");
                try {
                    asyncRequest = new XMLHttpRequest();

                    //register event handler
                    asyncRequest.onreadystatechange=stateChange;
                    var url="EmpDatabaseNew.php";
                    asyncRequest.open('GET',url,true);
                    asyncRequest.send(null);
                } 
                catch (exception) {alert(exception);}
            } 

            function clearEmployeeInfo(){
                document.getElementById("ContentArea").innerHTML = "";
            }

            function init(){
                document.getElementById("allEmployeesLink").addEventListener("mouseover", getEmployeeInfo);
                document.getElementById("allEmployeesLink").addEventListener("mouseout", clearEmployeeInfo);
            }

            document.addEventListener("DOMContentLoaded", init);

            function stateChange() {
                // if request completed successfully
                if(asyncRequest.readyState==4 && asyncRequest.status==200) {
                    document.getElementById("ContentArea").innerHTML=
                    asyncRequest.responseText;
                }
            }
                //jquery ajax
            //  $(document).ready(function(){
            //      $("#employeeDropDown").change(function(){
            //          var empID = $(this).val();
            //          $.ajax({
            //              url:"EmpDatabaseNew.php?empID=" + empID,
            //              async:true,
            //              success: function(result){
            //                  $("#ContentArea").html(result);
            //              }             
            //          });
            //      });
            //  });
        </script>
    </head>

   

      
    </br>
      </br>
    <body> 
        <!--Q1.1-->
        <!--Hover over the All Employees Link to show all the employees -->
        <medium><a id="allEmployeesLink" href="">All Employees</a></medium> 
        <br/>
        <br />
            <div id="ContentArea">&nbsp;</div> 
    </body>
</html>